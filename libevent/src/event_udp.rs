use libc;
use std::ffi;
use std::mem;
use std::ptr::null_mut;
use libc::{ c_void, c_short, c_int, size_t, ssize_t, socklen_t };

use event_utils::*;
use event_base::EventBase;
use global_utils::to_void_ptr;

extern fn on_event_callback(socket: c_int, events: c_short, context: *mut c_void) {
    if events & EV_READ > 0 {
        let ctx = unsafe { &*mem::transmute::<*mut c_void, *mut UdpEvent>(context) };
        (ctx.on_read_cb)(socket, ctx.context);
    }
}

extern "C" {
    fn evutil_closesocket(socket: c_int) -> c_int;
    fn evutil_make_socket_nonblocking(socket: c_int) -> c_int;
    fn socket(domain: c_int, socket_type: c_int, protocol: c_int) -> c_int;
    fn bind(socket: c_int, socket_address: *mut c_void, address_length: socklen_t) -> c_int;
    fn recvfrom(socket: c_int, buffer: *mut c_void, size: size_t, flags: c_int, address: *mut c_void,
        address_length: *mut c_void) -> ssize_t;
    fn sendto(socket: c_int, data: *const c_void, length: size_t, flags: c_int,
        address: *const c_void, address_length: socklen_t) -> ssize_t;
}

fn default_event_callback(_socket: c_int, _context: *mut c_void) {}

pub struct UdpEvent {
    port: u16,
    host: String,
    socket: c_int,
    event: *mut c_void,
    on_read_cb: fn(c_int, *mut c_void),
    context: *mut c_void,
}

impl UdpEvent {
    pub fn new(host: &str, port: u16) -> Result<Self> {
        debug!("UdpEvent()");
        let udp_port;
        let socket = unsafe {
            let socket: c_int = socket(libc::AF_INET, libc::SOCK_DGRAM, 0);
            if socket == -1 {
                error!("Socket initialization failed!");
                return Err(Error::ReturnCode);
            }
            if evutil_make_socket_nonblocking(socket) == -1 {
                error!("Make socket non blocking failed!");
                return Err(Error::ReturnCode);
            }

            let mut sockaddr: libc::sockaddr_in = mem::zeroed();
            sockaddr.sin_family = libc::AF_INET as libc::sa_family_t;
            sockaddr.sin_port = (port as u16).to_be();
            let mut rc = evutil_inet_pton(libc::AF_INET, ffi::CString::new(host).unwrap().as_ptr(),
                to_void_ptr(&mut sockaddr.sin_addr));
            if rc < 0 {
                error!("UDP event host address set failed!");
                return Err(Error::ReturnCode);
            }
            if bind(socket, to_void_ptr(&mut sockaddr), mem::size_of::<libc::sockaddr_in>() as socklen_t) == -1 {
                error!("Socket binding failed!");
                return Err(Error::ReturnCode);
            }
            let mut sock_len = mem::size_of::<libc::sockaddr_in>() as socklen_t;
            rc = libc::getsockname(socket, mem::transmute::<&mut libc::sockaddr_in, &mut libc::sockaddr>(&mut sockaddr),
                &mut sock_len);
            if rc < 0 {
                error!("UDP event get socket name failed!");
                return Err(Error::ReturnCode);
            }
            udp_port = sockaddr.sin_port.to_be();

            socket
        };

        Ok(
            UdpEvent {
                socket,
                port: udp_port,
                event: null_mut(),
                context: null_mut(),
                host: host.to_string(),
                on_read_cb: default_event_callback,
            }
        )
    }

    pub fn set_callback(&mut self, event_base: &mut EventBase, read_cb: fn(c_int, *mut c_void),
        context: *mut c_void) -> Result<()>
    {
        self.on_read_cb = read_cb;
        self.context = context;

        self.event = unsafe {
            let event = event_new(event_base.event_base, self.socket, EV_READ | EV_ET | EV_PERSIST,
                on_event_callback, to_void_ptr(self));
            if event == null_mut() {
                error!("UdpEvent initialization failed!");
                return Err(Error::ReturnCode);
            }
            if event_add(event, null_mut()) == -1 {
                error!("UdpEvent add failed!");
                return Err(Error::ReturnCode);
            }
            event
        };

        Ok(())
    }

    pub fn get_host(&self) -> &str {
        self.host.as_ref()
    }

    pub fn get_port(&self) -> u16 {
        self.port.clone()
    }

    pub fn read(&mut self, buffer: &mut Vec<u8>) -> isize {
        unsafe {
            let mut address_length: socklen_t = 0;
            recvfrom(self.socket, buffer.as_ptr() as *mut c_void, buffer.len() as size_t, 0,
                null_mut(), to_void_ptr(&mut address_length))
        }
    }

    pub fn write(&mut self, host: &str, port: u16, data: &Vec<u8>) -> Result<()> {
        unsafe {
            let mut sockaddr: libc::sockaddr_in = mem::zeroed();
            sockaddr.sin_family = libc::AF_INET as libc::sa_family_t;
            sockaddr.sin_port = port.to_be();
            let mut rc = evutil_inet_pton(libc::AF_INET, ffi::CString::new(host).unwrap().as_ptr(),
                to_void_ptr(&mut sockaddr.sin_addr));
            if rc < 0 {
                error!("Send to failed! Host not set!");
                return Err(Error::ReturnCode);
            }
            rc = sendto(self.socket, data.as_ptr() as *const c_void, data.len(), 0,
                to_void_ptr(&mut sockaddr), mem::size_of::<libc::sockaddr_in>() as socklen_t) as i32;
            if rc < 0 {
                error!("Send to failed!");
                return Err(Error::ReturnCode);
            }
        }

        Ok(())
    }
}

impl Drop for UdpEvent {
    fn drop(&mut self) {
        debug!("~UdpEvent()");
        unsafe {
            if self.event != null_mut() {
                event_free(self.event);
            }

            if self.socket > 0 {
                if self.socket > 0 {
                    if evutil_closesocket(self.socket) == -1 {
                        error!("Socket free error!");
                    }
                }
            }
        }
    }
}