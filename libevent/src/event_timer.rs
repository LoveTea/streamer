use std::mem;
use std::ptr::null_mut;
use libc::{ c_void, c_short, c_int, time_t, suseconds_t, timeval };

use event_utils::*;
use global_utils::*;

extern fn on_event_callback(socket: c_int, events: c_short, context: *mut c_void) {
    if events & EV_TIMEOUT > 0 {
        let ctx = unsafe { &*mem::transmute::<*mut c_void, *mut EventTimer>(context) };
        (ctx.callback)(socket, ctx.context);
    }
}

fn default_event_callback(_socket: c_int, _context: *mut c_void) {}

pub struct EventTimer {
    event: *mut c_void,
    callback: fn(c_int, *mut c_void),
    context: *mut c_void,
}

impl EventTimer {
    pub fn new() -> Self {
        debug!("EventTimer()");
        EventTimer {
            event: null_mut(),
            callback: default_event_callback,
            context: null_mut(),
        }
    }

    pub fn set_callback(&mut self, event_base: *mut c_void, cb: fn(c_int, *mut c_void),
        context: *mut c_void) -> Result<()>
    {
        self.event = unsafe {
            let event = event_new(event_base, -1, EV_TIMEOUT, on_event_callback, to_void_ptr(self));
            if event == null_mut() {
                error!("EventTimer initialization failed!");
                return Err(Error::ReturnCode);
            }
            event
        };
        self.callback = cb;
        self.context = context;

        Ok(())
    }

    pub fn start_timer(&mut self, secs: i32, microsecs: i32) -> Result<()> {
        unsafe {
            if self.event != null_mut() {
                event_del(self.event);

                let mut tv = timeval { tv_sec: secs as time_t, tv_usec: microsecs as suseconds_t };
                if event_add(self.event, &mut tv) == -1 {
                    error!("EventTimer initialization failed!");
                    return Err(Error::ReturnCode);
                }
            }
        }

        Ok(())
    }
}

unsafe impl Send for EventTimer {}

impl Drop for EventTimer {
    fn drop(&mut self) {
        debug!("~EventTimer()");
        if self.event != null_mut() {
            unsafe {
                event_free(self.event);
            };
        }
    }
}