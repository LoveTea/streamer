use libc;
use std::ffi;
use std::mem;
use std::cell;
use std::ptr::null_mut;
use libc::{ c_void, c_char, c_uchar, c_int, uint16_t };

use event_utils::*;
use event_base::EventBase;
use event_buffer::BufferEvent;
use global_utils::to_void_ptr;

type EvConnListenerCallback = extern fn(*const c_void, c_int, *const c_void, c_int, *const c_void);
type EvConnListenerErrorCallback = extern fn(*const c_void, *const c_void);

extern fn evconnlistener_callback(lev: *const c_void, socket: c_int, socket_address: *const c_void,
    _address_length: c_int, context: *const c_void)
{
    unsafe {
        let address: libc::sockaddr_in = *mem::transmute::<*const c_void, *const libc::sockaddr_in>(socket_address);
        let host = ffi::CStr::from_ptr(inet_ntoa(address.sin_addr));
        let mut port = address.sin_port as u16;
        port = port.to_le();
        let event_base = evconnlistener_get_base(lev);
        let bev = BufferEvent::new(event_base, socket, host.to_str().unwrap(), port).unwrap();
        let ctx = &*mem::transmute::<*const c_void, *const EventConnectionListener>(context);
        (ctx.on_connect_cb.get())(bev, ctx.cb_context.get());
    };
}

extern fn evconnlistener_error_callback(_lev: *const c_void, context: *const c_void) {
    let ctx = unsafe { &*mem::transmute::<*const c_void, *const EventConnectionListener>(context) };
    (ctx.on_error_cb.get())(ctx.cb_context.get());
}

#[link(name = "event")]
#[link(name = "event_pthreads")]
extern "C" {
    fn evconnlistener_new_bind(event_base: *mut c_void, evconnlistener_callback: EvConnListenerCallback,
        context: *mut c_void, flags: c_uchar, backlog: c_int, socket_address: *const c_void,
        address_length: c_int) -> *mut c_void;
    fn evconnlistener_set_cb(lev: *mut c_void, cb: EvConnListenerCallback, context: *mut c_void);
    fn evconnlistener_set_error_cb(lev: *mut c_void, cb: EvConnListenerErrorCallback);
    fn evconnlistener_get_base(lev: *const c_void) -> *mut c_void;
    fn evconnlistener_free(evconnlistener: *mut c_void);
    fn inet_ntoa(in_address: libc::in_addr) -> *mut c_char;
}

fn default_connect_cb(_bev: BufferEvent, _context: *const c_void) {}
fn default_error_cb(_context: *const c_void) {}

pub struct EventConnectionListener {
    pub(crate) lev: *mut c_void,
    on_connect_cb: cell::Cell<fn(BufferEvent, *const c_void)>,
    on_error_cb: cell::Cell<fn(*const c_void)>,
    cb_context: cell::Cell<*const c_void>,
}

impl EventConnectionListener {
    pub fn new(event_base: &EventBase, host: &str, port: uint16_t) -> Result<Self>
    {
        debug!("EventConnectionListener()");
        let mut sockaddr: libc::sockaddr_in = unsafe { mem::zeroed() };
        sockaddr.sin_family = libc::AF_INET as libc::sa_family_t;
        sockaddr.sin_port = (port as u16).to_be();
        let lev = unsafe {
            if !host.is_empty() {
                evutil_inet_pton(libc::AF_INET, ffi::CString::new(host).unwrap().as_ptr(),
                    to_void_ptr(&mut sockaddr.sin_addr));
            }
            evconnlistener_new_bind(event_base.event_base, evconnlistener_callback, null_mut(),
                LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE, -1, to_void_ptr(&mut sockaddr),
                mem::size_of::<libc::sockaddr_in>() as c_int)
        };

        if lev == null_mut() {
            error!("Libevent connection listener init failed!");
            Err(Error::StructureInit)
        } else {
            Ok(
                EventConnectionListener {
                    lev,
                    on_connect_cb: cell::Cell::new(default_connect_cb),
                    on_error_cb: cell::Cell::new(default_error_cb),
                    cb_context: cell::Cell::new(null_mut()),
                }
            )
        }
    }

    pub fn set_callback(&mut self, on_connect_cb: fn(BufferEvent, *const c_void),
        on_error_cb: fn(*const c_void), cb_context: *const c_void)
    {
        self.on_connect_cb.set(on_connect_cb);
        self.on_error_cb.set(on_error_cb);
        self.cb_context.set(cb_context);
        unsafe {
            evconnlistener_set_cb(self.lev, evconnlistener_callback, to_void_ptr(self));
            evconnlistener_set_error_cb(self.lev, evconnlistener_error_callback);
        }
    }
}

impl Drop for EventConnectionListener {
    fn drop(&mut self) {
        debug!("~EventConnectionListener()");
        if self.lev != null_mut() {
            unsafe {
                evconnlistener_free(self.lev);
            }
        }
    }
}