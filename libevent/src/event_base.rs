use libc;
use std::ptr::null_mut;
use libc::{ c_void, c_int };

use event_utils::*;

extern "C" {
    fn event_base_new() -> *mut c_void;
    fn event_base_free(event_base: *const c_void);
    fn event_base_dispatch(event_base: *const c_void) -> c_int;
    fn evthread_use_pthreads() -> c_int;
    fn evthread_enable_lock_debugging() -> c_int;
}

pub struct EventBase {
    pub(crate) event_base: *mut c_void,
}

impl EventBase {
    pub fn new() -> Result<Self> {
        debug!("EventBase()");
        let base = unsafe {
            let mut rc = evthread_enable_lock_debugging();
            if rc == 0 {
                rc = evthread_use_pthreads();
            }
            if rc != 0 {
                error!("Libevent multithreading init failed!");
                return Err(Error::ReturnCode);
            }

            event_base_new()
        };
        if base == null_mut() {
            error!("Libevent event base init failed!");
            Err(Error::StructureInit)
        } else {
            Ok(EventBase { event_base: base })
        }
    }

    pub fn dispatch(&self) -> Result<()> {
        debug!("Dispatch");
        let rc: i32 = unsafe {
            libc::signal(libc::SIGPIPE, libc::SIG_IGN);
            event_base_dispatch(self.event_base)
        };
        if rc == -1 {
            error!("Libevent event dispatch failed!");
            Err(Error::ReturnCode)
        } else {
            Ok(())
        }
    }

    pub fn get_event_base(&mut self) -> *mut c_void {
        self.event_base
    }
}

impl Drop for EventBase {
    fn drop(&mut self) {
        debug!("~EventBase()");
        if self.event_base != null_mut() {
            unsafe {
                event_base_free(self.event_base);
            }
        }
    }
}