use std::fmt;
use std::error;
use std::result;
use libc::{ c_void, c_char, c_uchar, c_short, c_int, timeval };

// event flags
pub const EV_TIMEOUT: c_short = 0x01;
pub const EV_READ: c_short = 0x02;
pub const EV_WRITE: c_short = 0x04;
pub const EV_PERSIST: c_short = 0x10;
pub const EV_ET: c_short = 0x20;

// connection listener options
pub const LEV_OPT_CLOSE_ON_FREE: c_uchar = 0x02;
pub const LEV_OPT_REUSEABLE: c_uchar = 0x08;
pub const LEV_OPT_THREADSAFE: c_uchar = 0x10;

// buffer event options
pub const BEV_OPT_CLOSE_ON_FREE: c_int = 0x01;
pub const BEV_OPT_THREADSAFE: c_int = 0x02;

//buffer event events
pub const BEV_EVENT_EOF: c_short = 0x10;
pub const BEV_EVENT_ERROR: c_short = 0x20;
pub const BEV_EVENT_TIMEOUT: c_short = 0x40;

type EventCallback = extern fn(c_int, c_short, *mut c_void);

extern "C" {
    pub(crate) fn evutil_inet_pton(af: c_int, src: *const c_char, dst: *const c_void) -> c_int;
    pub(crate) fn event_new(event_base: *mut c_void, socket: c_int, events: c_short, callback: EventCallback,
        context: *mut c_void) -> *mut c_void;
    pub(crate) fn event_add(event: *mut c_void, tv: *mut timeval) -> c_int;
    pub(crate) fn event_free(event: *mut c_void);
    pub(crate) fn event_del(event: *mut c_void) ->c_int;
}

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    StructureInit,
    ReturnCode,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::StructureInit => "Event base initialization failure!",
            Error::ReturnCode => "C function returned failure code!",
        }
    }
}