use std::ptr;
use std::mem;
use std::ptr::null_mut;
use libc::{ c_void, c_uchar, c_short, c_int, size_t };

use event_utils::*;
use global_utils::to_void_ptr;

type BufferEventDataCallback = extern fn(*mut c_void, *mut c_void);
type BufferEventEventCallback = extern fn(*mut c_void, c_short, *mut c_void);

extern fn read_cb(_bev: *mut c_void, context: *mut c_void) {
    let bev = unsafe { &*mem::transmute::<*mut c_void, *mut BufferEvent>(context) };
    (bev.on_read_cb)(bev, bev.cb_context);
}

extern fn write_cb(_bev: *mut c_void, _context: *mut c_void) {}

extern fn event_cb(_bev: *mut c_void, events: c_short, context: *mut c_void) {
    if events & BEV_EVENT_EOF > 0 || events & BEV_EVENT_ERROR > 0 || events & BEV_EVENT_TIMEOUT > 0 {
        let bev = unsafe { &*mem::transmute::<*mut c_void, *mut BufferEvent>(context) };
        (bev.on_disconnect_cb)(bev, bev.cb_context);
    }
}

extern "C" {
    fn bufferevent_socket_new(event_base: *mut c_void, socket: c_int, options: c_int) -> *mut c_void;
    fn bufferevent_setcb(bev: *mut c_void, read_cb: BufferEventDataCallback, write_cb: BufferEventDataCallback,
        event_cb: BufferEventEventCallback, context: *mut c_void);
    fn bufferevent_enable(bev: *mut c_void, events: c_short);
    fn bufferevent_get_input(bev: *const c_void) -> *mut c_void;
    fn bufferevent_write(bev: *mut c_void, data: *const c_void, length: size_t) -> c_int;
    fn bufferevent_get_base(bev: *mut c_void) -> *mut c_void;
    fn bufferevent_free(bev: *mut c_void) -> c_int;

    fn evbuffer_get_length(buf: *const c_void) -> size_t;
    fn evbuffer_pullup(buf: *mut c_void, length: size_t) -> *const c_uchar;
    fn evbuffer_drain(buf: *mut c_void, length: size_t) -> c_int;
}

fn default_cb(_bev: &BufferEvent, _context: *mut c_void) {}

pub struct BufferEvent {
    port: u16,
    host: String,
    cb_context: *mut c_void,
    pub(crate) bev: *mut c_void,
    on_read_cb: fn(&BufferEvent, *mut c_void),
    on_disconnect_cb: fn(&BufferEvent, *mut c_void),
}

impl BufferEvent {
    pub(crate) fn new(event_base: *const c_void, socket: c_int, host: &str, port: u16) -> Result<Self> {
        debug!("BufferEvent({}, {}, {})", socket, host, port);
        let bev = unsafe { bufferevent_socket_new(event_base as *mut c_void, socket,
            BEV_OPT_CLOSE_ON_FREE | BEV_OPT_THREADSAFE) };
        if bev == null_mut() {
            error!("Libevent buffer event init failed!");
            Err(Error::StructureInit)
        } else {
            Ok(
                BufferEvent {
                    bev,
                    port,
                    host: host.to_string(),
                    cb_context: null_mut(),
                    on_read_cb: default_cb,
                    on_disconnect_cb: default_cb,
               }
            )
        }
    }

    pub fn set_callback(&mut self, on_read_cb: fn(&BufferEvent, *mut c_void),
        on_disconnect_cb: fn(&BufferEvent, *mut c_void), cb_context: *mut c_void)
    {
        self.on_read_cb = on_read_cb;
        self.on_disconnect_cb = on_disconnect_cb;
        self.cb_context = cb_context;

        unsafe {
            bufferevent_setcb(self.bev, read_cb, write_cb, event_cb, to_void_ptr(self));
        };
    }

    pub fn get_host(&self) -> &str {
        self.host.as_ref()
    }

    pub fn get_port(&self) -> u16 {
        self.port
    }

    pub fn enable(&self) {
        unsafe {
            bufferevent_enable(self.bev, EV_READ);
        };
    }

    pub fn bytes_available(&self) -> usize {
        unsafe {
            evbuffer_get_length(bufferevent_get_input(self.bev))
        }
    }

    pub fn read(&self, data: *mut u8, length: usize) -> Result<()> {
        unsafe {
            if self.bev == null_mut() {
                return Err(Error::ReturnCode);
            }

            let buf: *mut c_void = bufferevent_get_input(self.bev);
            let src = evbuffer_pullup(buf, length);

            if src == null_mut() {
                error!("Read buffer event failed!");
                Err(Error::ReturnCode)
            } else {
                ptr::copy(src, data, length);
                evbuffer_drain(buf, length);
                Ok(())
            }
        }
    }

    pub fn write(&self, data: *const u8, length: usize) -> Result<()> {
        let rc = unsafe {
            bufferevent_write(self.bev, data as *const c_void, length)
        };

        if rc == 0 {
            Ok(())
        } else {
            error!("Write to buffer event failed!");
            Err(Error::ReturnCode)
        }
    }

    pub fn get_event_base(&mut self) -> *mut c_void {
        unsafe {
            bufferevent_get_base(self.bev)
        }
    }
}

unsafe impl Send for BufferEvent {}

impl Drop for BufferEvent {
    fn drop(&mut self) {
        debug!("~BufferEvent()");
        if self.bev != null_mut() {
            unsafe {
                bufferevent_free(self.bev);
                self.bev = null_mut();
            };
        }
    }
}