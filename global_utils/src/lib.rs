extern crate libc;

pub fn to_void_ptr<T>(val: &mut T) -> *mut ::libc::c_void {
    unsafe {
        ::std::mem::transmute::<*const T, *mut ::libc::c_void>(val)
    }
}