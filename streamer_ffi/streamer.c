#include <malloc.h>
#include <libavformat/avformat.h>

#include "streamer.h"

#define PRINT_ERROR(...) fprintf(stderr, __VA_ARGS__)

#define MAX_STRING_BUFFER_LENGTH 256

typedef struct H264Params {
    uint8_t version;
    uint8_t profile;
    uint8_t compatibility;
    uint8_t level;
    uint8_t nal_size_length;
    uint8_t sps_count;
    uint8_t pps_count;
    uint16_t sps_size;
    uint16_t pps_size;
    uint8_t sps[MAX_STRING_BUFFER_LENGTH];
    uint8_t pps[MAX_STRING_BUFFER_LENGTH];
} H264Params;

typedef struct StreamerContext {
	char input_file[MAX_STRING_BUFFER_LENGTH];
	const char* file_format;

	int packet_pos;
    int end_of_file;

    int mp3_stream_index;
    int h264_stream_index;

	AVPacket* packet;
	AVFormatContext* format;
	H264Params h264_params;
} StreamerContext;

int parse_h264_params(StreamerContext* ctx) {
	if (ctx->format->streams[ctx->h264_stream_index]->codecpar->extradata_size) {
		uint8_t* params = ctx->format->streams[ctx->h264_stream_index]->codecpar->extradata;
		ctx->h264_params.version = *params; ++params;
		ctx->h264_params.profile = *params; ++params;
		ctx->h264_params.compatibility = *params; ++params;
		ctx->h264_params.level = *params; ++params;
		ctx->h264_params.nal_size_length = (uint8_t) ((0x03 & *params) + 1); ++params;
		ctx->h264_params.sps_count = (uint8_t) 0x1f & *params; ++params;
		if (ctx->h264_params.sps_count > 1) {
			PRINT_ERROR("Count of sequence parameter set more than one!");
			return -1;
		}
		ctx->h264_params.sps_size = *params << 8 | *(params + 1); params += 2;
		if (ctx->h264_params.sps_size > MAX_STRING_BUFFER_LENGTH) {
			PRINT_ERROR("Sequence parameter set to big!");
			return -1;
		}
		if (ctx->h264_params.sps_count) {
			memcpy(ctx->h264_params.sps, params, ctx->h264_params.sps_size); params += ctx->h264_params.sps_size;
		}
		ctx->h264_params.pps_count = *params; ++params;
		if (ctx->h264_params.sps_count > 1) {
			PRINT_ERROR("Count of picture parameter set more than one!");
			return -1;
		}
		ctx->h264_params.pps_size = *params << 8 | *(params + 1); params += 2;
		if (ctx->h264_params.sps_size > MAX_STRING_BUFFER_LENGTH) {
			PRINT_ERROR("Picture parameter set to big!");
			return -1;
		}
		if (ctx->h264_params.pps_count) {
			memcpy(ctx->h264_params.pps, params, ctx->h264_params.pps_size); params += ctx->h264_params.pps_size;
		}
	}

	return 0;
}

int next_packet(StreamerContext* ctx) {
	do {
		int rc = av_read_frame(ctx->format, ctx->packet);
		if (rc) {
		    if (rc == AVERROR_EOF) {
		        ctx->end_of_file = 1;
		    }
			PRINT_ERROR("Get NAL failed! %s\n", av_err2str(rc));
			return -1;
		}
	} while (ctx->packet->stream_index != ctx->h264_stream_index && ctx->packet->stream_index != ctx->mp3_stream_index);

	return 0;
}

int nal_start_search(const uint8_t* begin, const uint8_t* end, uint8_t* delimiter_length) {
	*delimiter_length = 0;
	const uint8_t* pos = begin;

	while (end - pos >= 4) {
		if (*pos == 0 && *(pos + 1) == 0) {
			if (*(pos + 2) == 1) {
				*delimiter_length = 3;
				return (int) (pos - begin);
			} else if (*(pos + 2) == 0 && *(pos + 3) == 1) {
				*delimiter_length = 4;
				return (int) (pos - begin);
			}
		}

		++pos;
	}

	return -1;
}

int get_nal_annex_b(StreamerContext* ctx, const uint8_t** out) {
    if (ctx->packet->stream_index != ctx->h264_stream_index) {
        return -1;
    }

	int nal_size;
	uint8_t start_length;
	int nal_begin = nal_start_search(ctx->packet->data + ctx->packet_pos, ctx->packet->data + ctx->packet->size,
		&start_length);
	if (nal_begin < 0) {
		PRINT_ERROR("NAL delimiter not found!\n");
		return -1;
	}
	nal_begin += start_length;
	uint8_t end_length;
	int nal_end = nal_start_search(ctx->packet->data + ctx->packet_pos + nal_begin, ctx->packet->data + ctx->packet->size,
		&end_length);

	*out = ctx->packet->data + ctx->packet_pos + nal_begin;
	if (nal_end < 0) {
		nal_size = ctx->packet->size - ctx->packet_pos - nal_begin;
	} else {
		nal_size = nal_end - nal_begin;
	}

	ctx->packet_pos += nal_size + start_length;

	return nal_size;
}

int get_nal_avcc(StreamerContext* ctx, const uint8_t** out) {
	if (ctx->packet->size - ctx->packet_pos < 4) {
		return -1;
	}

	int nal_size = 0;
	switch (ctx->h264_params.nal_size_length) {
		case 4:
			nal_size |= *(ctx->packet->data + ctx->packet_pos) << 24;
		case 3:
			nal_size |= *(ctx->packet->data + ctx->packet_pos + 1) << 16;
		case 2:
			nal_size |= *(ctx->packet->data + ctx->packet_pos + 2) << 8;
		case 1:
			nal_size |= *(ctx->packet->data + ctx->packet_pos + 3);
		default:
			break;
	}
	*out = ctx->packet->data + ctx->packet_pos + ctx->h264_params.nal_size_length;
	ctx->packet_pos += nal_size + ctx->h264_params.nal_size_length;

	return nal_size;
}

int get_nal_mp3(StreamerContext* ctx, const uint8_t** out) {
    if (ctx->packet->stream_index != ctx->mp3_stream_index) {
        return -1;
    }

    int nal_size = ctx->packet->size;
    *out = ctx->packet->data + ctx->packet_pos;
    ctx->packet_pos += nal_size;

    return nal_size;
}



// ------------------------- PUBLIC API ----------------------------

StreamerContext* stream_context_new(const char* input_file) {
	StreamerContext* ctx = calloc(1, sizeof(StreamerContext));
	if (!ctx) {
		PRINT_ERROR("Stream context malloc error!\n");
        stream_context_delete(ctx);
		return NULL;
	}

	if (strlen(input_file) > MAX_STRING_BUFFER_LENGTH) {
		PRINT_ERROR("File path to long!");
		return NULL;
	}

	strcpy(ctx->input_file, input_file);
	ctx->file_format = NULL;
	ctx->packet_pos = 0;
	ctx->end_of_file = 0;

    ctx->mp3_stream_index = -1;
    ctx->h264_stream_index = -1;

	char* pos = strrchr(ctx->input_file, '.');
	if (pos) {
		ctx->file_format = pos + 1;
	}

	int rc = avformat_open_input(&ctx->format, ctx->input_file, NULL, NULL);
	if (rc) {
		PRINT_ERROR("Open format context failed! %s\n", av_err2str(rc));
		stream_context_delete(ctx);
		return NULL;
	}
	rc = avformat_find_stream_info(ctx->format, NULL);
	if (rc < 0) {
		PRINT_ERROR("Find stream info failed! %s\n", av_err2str(rc));
		stream_context_delete(ctx);
		return NULL;
	}

#ifdef DEBUG
	av_dump_format(ctx->format, 0, ctx->input_file, 0);
#endif

	for (int i = 0; i < ctx->format->nb_streams; ++i) {
		if (ctx->format->streams[i]->codecpar->codec_id == AV_CODEC_ID_H264) {
			ctx->h264_stream_index = i;
		}

		if (ctx->format->streams[i]->codecpar->codec_id == AV_CODEC_ID_MP3) {
		    ctx->mp3_stream_index = i;
		}
	}
	if (ctx->h264_stream_index < 0) {
		PRINT_ERROR("File doesn't contains h264 stream!");
		stream_context_delete(ctx);
		return NULL;
	}
	if (ctx->mp3_stream_index < 0) {
        PRINT_ERROR("File doesn't contains mp3 stream!");
        stream_context_delete(ctx);
        return NULL;
	}

	if (parse_h264_params(ctx)) {
		stream_context_delete(ctx);
		return NULL;
	}

	ctx->packet = av_packet_alloc();
	if (!ctx->packet) {
		PRINT_ERROR("Packet allocation failed!\n");
		stream_context_delete(ctx);
		return NULL;
	}

	return ctx;
}

int stream_get_video_frequency(StreamerContext* ctx) {
	return ctx->format->streams[ctx->h264_stream_index]->time_base.den / ctx->format->streams[ctx->h264_stream_index]->time_base.num;
}

int stream_get_audio_frequency(StreamerContext* ctx) {
	return ctx->format->streams[ctx->mp3_stream_index]->time_base.den / ctx->format->streams[ctx->mp3_stream_index]->time_base.num;
}

float stream_get_frame_rate(StreamerContext* ctx) {
	return ctx->format->streams[ctx->h264_stream_index]->r_frame_rate.num / ctx->format->streams[ctx->h264_stream_index]->r_frame_rate.den;
}

int stream_get_width(StreamerContext* ctx) {
	return ctx->format->streams[ctx->h264_stream_index]->codecpar->width;
}

int stream_get_height(StreamerContext* ctx) {
	return ctx->format->streams[ctx->h264_stream_index]->codecpar->height;
}

int64_t stream_get_duration(StreamerContext* ctx) {
	int64_t duration = ctx->format->streams[ctx->h264_stream_index]->duration;
	return duration > 0 ? duration : 0;
}

int64_t stream_get_bit_rate(StreamerContext* ctx) {
	return ctx->format->streams[ctx->h264_stream_index]->codecpar->bit_rate;
}

int stream_get_version(StreamerContext* ctx) {
	if (!ctx->h264_params.sps_size) {
		return -1;
	}

	return ctx->h264_params.version;
}

int stream_get_profile(StreamerContext* ctx) {
	if (!ctx->h264_params.sps_size) {
		return -1;
	}

	return ctx->h264_params.profile;
}

int stream_get_compatibility(StreamerContext* ctx) {
	if (!ctx->h264_params.sps_size) {
		return -1;
	}

	return ctx->h264_params.compatibility;
}

int stream_get_level(StreamerContext* ctx) {
	if (!ctx->h264_params.sps_size) {
		return -1;
	}

	return ctx->h264_params.level;
}

int stream_get_sps(StreamerContext* ctx, const uint8_t** out) {
	if (!ctx->h264_params.sps_size) {
		return -1;
	}

	*out = ctx->h264_params.sps;
	return ctx->h264_params.sps_size;
}

int stream_get_pps(StreamerContext* ctx, const uint8_t** out) {
	if (!ctx->h264_params.pps_size) {
		return -1;
	}

	*out = ctx->h264_params.pps;
	return ctx->h264_params.pps_size;
}

int stream_seek(StreamerContext* ctx, int64_t pts) {
	int flags = AVSEEK_FLAG_FRAME;
	if (ctx->packet->pts > pts) {
		flags |= AVSEEK_FLAG_BACKWARD;
	}

	int rc = av_seek_frame(ctx->format, ctx->h264_stream_index, pts, flags);
	if (rc >= 0) {
		rc = next_packet(ctx);
		ctx->packet_pos = 0;
		ctx->end_of_file = 0;
	}

	return rc;
}

int stream_get_nal(StreamerContext* ctx, const uint8_t** out, int64_t* pts, NalType* type) {
#ifdef DEBUG
	static int once = 0;
#endif
	if (ctx->end_of_file) {
        return -1;
	}

	if (ctx->packet_pos >= ctx->packet->size) {
		int rc = next_packet(ctx);
		if (rc) {
			return rc;
		}
		ctx->packet_pos = 0;
#ifdef DEBUG
		once = 0;
#endif
	}

#ifdef DEBUG
	if (!once) {
		printf("-------------- FRAME PARAMS -----------------\n");
		printf("STREAM: %d; TIME BASE %d %d\n", ctx->packet->stream_index,
		    ctx->format->streams[ctx->packet->stream_index]->time_base.num,
		    ctx->format->streams[ctx->packet->stream_index]->time_base.den);
		for (int i = 0; i < ctx->format->streams[ctx->packet->stream_index]->codecpar->extradata_size; ++i) {
			if (i != 0 && i % 75 == 0) {
				printf("\n");
			}
			printf("%02x ", ctx->format->streams[ctx->packet->stream_index]->codecpar->extradata[i]);
		}
		printf("\n\n");

		printf("-------------- FRAME ------------------\n");
		for (int i = 0; i < ctx->packet->size; ++i) {
			if (i != 0 && i % 75 == 0) {
				printf("\n");
			}
			printf("%02x ", ctx->packet->data[i]);
		}

		printf("\n\n\n");
		++once;
	}
#endif

	*pts = ctx->packet->pts;
	if (ctx->packet->stream_index == ctx->h264_stream_index) {
        *type = NAL_VIDEO;
        if (ctx->format->streams[ctx->h264_stream_index]->codecpar->extradata_size > 0) {
            return get_nal_avcc(ctx, out);
        } else {
            return get_nal_annex_b(ctx, out);
        }
	} else {
        *type = NAL_AUDIO;
        return get_nal_mp3(ctx, out);
	}
}

void stream_context_delete(StreamerContext* ctx) {
	if (!ctx) {
		return;
	}

	if (ctx->format) {
		avformat_close_input(&ctx->format);
	}

	if (ctx->packet) {

	}

	free(ctx);
}