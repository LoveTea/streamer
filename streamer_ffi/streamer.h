#ifndef PLAYER_H264_STREAMER_H
#define PLAYER_H264_STREAMER_H

#include <stdint.h>

typedef enum  {
    NAL_VIDEO,
    NAL_AUDIO
} NalType;

typedef struct StreamerContext StreamerContext;

StreamerContext* stream_context_new(const char* input_file);

int stream_get_video_frequency(StreamerContext* ctx);
int stream_get_audio_frequency(StreamerContext* ctx);
float stream_get_frame_rate(StreamerContext* ctx);
int stream_get_width(StreamerContext* ctx);
int stream_get_height(StreamerContext* ctx);
int64_t stream_get_duration(StreamerContext* ctx);
int64_t stream_get_bit_rate(StreamerContext* ctx);
int stream_get_version(StreamerContext* ctx);
int stream_get_profile(StreamerContext* ctx);
int stream_get_compatibility(StreamerContext* ctx);
int stream_get_level(StreamerContext* ctx);
int stream_get_sps(StreamerContext* ctx, const uint8_t** out);
int stream_get_pps(StreamerContext* ctx, const uint8_t** out);
int stream_seek(StreamerContext* ctx, int64_t pts);
int stream_get_nal(StreamerContext* ctx, const uint8_t** out, int64_t* pts, NalType* type);

void stream_context_delete(StreamerContext* ctx);

#endif //PLAYER_H264_STREAMER_H
