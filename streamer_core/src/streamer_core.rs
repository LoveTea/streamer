use std::ffi::CString;
use std::collections::VecDeque;
use std::ptr::{ copy, null, null_mut };
use libc::{ c_void, c_char, c_int, c_float, int64_t };

use streamer_core_utils::*;

#[repr(C)]
#[derive(Debug, Clone, PartialEq)]
pub enum NalType {
    NalVideo,
    NalAudio,
}

extern "C" {
    fn stream_context_new(input_file: *const c_char) -> *mut c_void;
    fn stream_get_video_frequency(context: *mut c_void) -> c_int;
    fn stream_get_audio_frequency(context: *mut c_void) -> c_int;
    fn stream_get_frame_rate(context: *mut c_void) -> c_float;
    fn stream_get_width(context: *mut c_void) -> c_int;
    fn stream_get_height(context: *mut c_void) -> c_int;
    fn stream_get_duration(context: *mut c_void) -> int64_t;
    fn stream_get_bit_rate(context: *mut c_void) -> int64_t;
    fn stream_get_version(context: *mut c_void) -> c_int;
    fn stream_get_profile(contxt: *mut c_void) -> c_int;
    fn stream_get_compatibility(contxt: *mut c_void) -> c_int;
    fn stream_get_level(contxt: *mut c_void) -> c_int;
    fn stream_get_sps(context: *mut c_void, out: *mut *const c_void) -> c_int;
    fn stream_get_pps(context: *mut c_void, out: *mut *const c_void) -> c_int;
    fn stream_seek(context: *mut c_void, pts: int64_t) -> c_int;
    fn stream_get_nal(context: *mut c_void, out: *mut *const c_void, pts: *mut int64_t,
        nal_type: *mut NalType) -> c_int;
    fn stream_context_delete(context: *mut c_void);
}

struct Nal {
    nal: Vec<u8>,
    pts: i64,
}

pub struct Streamer {
    send_timeout: i32,
    context: *mut c_void,
    buffered_video_nals: VecDeque<Nal>,
    buffered_audio_nals: VecDeque<Nal>,
}

impl Streamer {
    pub fn new(file_name: &str) -> Result<Self> {
        let context = unsafe {
            stream_context_new(CString::new(file_name).unwrap().as_ptr())
        };
        if context == null_mut() {
            error!("Stream context init error!");
            return Err(Error::StructureInit);
        }

        let frame_rate = unsafe {
            stream_get_frame_rate(context)
        } as i32;

        let mut send_timeout = 30000;
        if frame_rate > 0 {
            send_timeout = 1000000 / frame_rate;
        }

        Ok(
            Streamer {
                context,
                send_timeout,
                buffered_video_nals: VecDeque::new(),
                buffered_audio_nals: VecDeque::new(),
            }
        )
    }

    pub fn get_send_timeout_ms(&self) -> i32 {
        self.send_timeout
    }

    pub fn get_video_frequency(&self) -> i32 {
        unsafe {
            stream_get_video_frequency(self.context)
        }
    }

    pub fn get_audio_frequency(&self) -> i32 {
        unsafe {
            stream_get_audio_frequency(self.context)
        }
    }

    pub fn get_frame_rate(&self) -> f32 {
        unsafe {
            stream_get_frame_rate(self.context)
        }
    }

    pub fn get_width(&self) -> i32 {
        unsafe {
            stream_get_width(self.context)
        }
    }

    pub fn get_height(&self) -> i32 {
        unsafe {
            stream_get_height(self.context)
        }
    }

    pub fn get_duration(&self) -> i64 {
        unsafe {
            stream_get_duration(self.context)
        }
    }

    pub fn get_bit_rate(&self) -> i64 {
        unsafe {
            stream_get_bit_rate(self.context)
        }
    }

    pub fn get_version(&self) -> i32 {
        unsafe {
            stream_get_version(self.context)
        }
    }

    pub fn get_profile(&self) -> i32 {
        unsafe {
            stream_get_profile(self.context)
        }
    }

    pub fn get_compatibility(&self) -> i32 {
        unsafe {
            stream_get_compatibility(self.context)
        }
    }

    pub fn get_level(&self) -> i32 {
        unsafe {
            stream_get_level(self.context)
        }
    }

    pub fn get_sps(&self) -> Vec<u8> {
        unsafe {
            let mut sps = vec![0u8];
            let mut out = null();
            let sps_size = stream_get_sps(self.context, &mut out);
            sps.resize(sps_size as usize, 0);
            copy(out, sps.as_mut_ptr() as *mut c_void, sps_size as usize);
            sps
        }
    }

    pub fn get_pps(&self) -> Vec<u8> {
        unsafe {
            let mut pps = vec![0u8];
            let mut out = null();
            let pps_size = stream_get_pps(self.context, &mut out);
            pps.resize(pps_size as usize, 0);
            copy(out, pps.as_mut_ptr() as *mut c_void, pps_size as usize);
            pps
        }
    }

    pub fn seek(&mut self, pts: i64) -> Result<()> {
        unsafe {
            if stream_seek(self.context, pts) < 0 {
                return Err(Error::ReturnCode);
            }
        }

        self.buffered_audio_nals.clear();
        self.buffered_video_nals.clear();

        Ok(())
    }

    pub fn get_next_video_pts(&mut self) -> Result<i64> {
        if !self.buffered_video_nals.is_empty() {
            for nal in &self.buffered_video_nals {
                return Ok(nal.pts);
            }
        }

        loop {
            let (nal, pts, nal_type) = self.get_next_nal()?;
            if nal_type == NalType::NalAudio {
                self.buffered_audio_nals.push_back(Nal { nal, pts });
                continue;
            } else {
                self.buffered_video_nals.push_back(Nal { nal, pts });
            }

            let nal_ref = self.buffered_video_nals.back_mut().unwrap();
            return Ok(nal_ref.pts);
        }
    }

    pub fn get_next_audio_pts(&mut self) -> Result<i64> {
        if !self.buffered_audio_nals.is_empty() {
            for nal in &self.buffered_audio_nals {
                return Ok(nal.pts);
            }
        }

        loop {
            let (nal, pts, nal_type) = self.get_next_nal()?;
            if nal_type == NalType::NalVideo {
                self.buffered_video_nals.push_back(Nal { nal, pts });
                continue;
            } else {
                self.buffered_audio_nals.push_back(Nal { nal, pts });
            }

            let nal_ref = self.buffered_audio_nals.back_mut().unwrap();
            return Ok(nal_ref.pts);
        }
    }

    pub fn get_video_nal(&mut self) -> Result<(Vec<u8>, i64)> {
        if !self.buffered_video_nals.is_empty() {
            let nal = self.buffered_video_nals.pop_front().unwrap();
            return Ok((nal.nal, nal.pts));
        }

        loop {
            let (nal, pts, nal_type) = self.get_next_nal()?;
            if nal_type == NalType::NalAudio {
                self.buffered_audio_nals.push_back(Nal { nal, pts });
                continue;
            }

            return Ok((nal, pts));
        }
    }

    pub fn get_audio_nal(&mut self) -> Result<(Vec<u8>, i64)> {
        if !self.buffered_audio_nals.is_empty() {
            let nal = self.buffered_audio_nals.pop_front().unwrap();
            return Ok((nal.nal, nal.pts));
        }

        loop {
            let (nal, pts, nal_type) = self.get_next_nal()?;
            if nal_type == NalType::NalVideo {
                self.buffered_video_nals.push_back(Nal { nal, pts });
                continue;
            }

            return Ok((nal, pts));
        }
    }

    fn get_next_nal(&mut self) -> Result<(Vec<u8>, i64, NalType)> {
        let mut nal = Vec::new();
        let mut pts = 0i64;
        let mut nal_type = NalType::NalVideo;
        unsafe {
            let mut out = null();
            let nal_size = stream_get_nal(self.context, &mut out, &mut pts, &mut nal_type);
            if nal_size < 0 {
                error!("NAL get failed!");
                return Err(Error::ReturnCode);
            }
            nal.resize(nal_size as usize, 0);
            copy(out, nal.as_mut_ptr() as *mut c_void, nal_size as usize);
        };

        Ok((nal, pts, nal_type))
    }
}

impl Drop for Streamer {
    fn drop(&mut self) {
        if self.context != null_mut() {
            unsafe {
                stream_context_delete(self.context);
            };
        }
    }
}