#[macro_use]
extern crate log;
extern crate libc;

#[link(name = "streamer_c")]
pub mod streamer_core;
pub mod streamer_core_utils;
