use std::fmt;
use std::error;
use std::result;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    EndOfFile,
    ReturnCode,
    StructureInit,
    OptionNotFound,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::EndOfFile => "End of file",
            Error::ReturnCode => "C function returned failure code!",
            Error::StructureInit => "Event base initialization failure!",
            Error::OptionNotFound => "Option not found!",
        }
    }
}