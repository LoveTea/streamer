use url;
use std::num;
use std::str;
use std::fmt;
use std::error;
use std::result;
use std::convert;

pub const NEW_LINE: &str = "\r\n";

pub const ALLOWED_VERSIONS: &[&str] = &["RTSP/1.0", "RTSP/2.0"];

pub const ALLOWED_COMMANDS: &[&str] = &["DESCRIBE", "OPTIONS", "SETUP", "PLAY", "GET_PARAMETER",
    "PAUSE", "TEARDOWN"];
pub const COMMANDS: &[&str] = &["DESCRIBE", "GET_PARAMETER", "OPTIONS", "PAUSE", "PLAY",
    "PLAY_NOTIFY", "REDIRECT", "SETUP", "SET_PARAMETER", "TEARDOWN"];

pub const ALLOWED_HEADERS: &[&str] = &["CSeq", "Date", "Public", "Server", "Content-Type", "Seek-Style",
    "Content-Length", "Session", "Transport", "RTP-Info", "Range", "Cache-Control", "Accept-Ranges"];

pub const ALLOWED_CODES: &[i32] = &[200, 500];

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    BadUrl,
    NotFound,
    Serialize,
    Deserialize,
    StartString,
    StructureInit,
    CodeNotAllowed,
    NotImplemented,
    HeaderNotAllowed,
    UrlParse(url::ParseError),
    DeserializeUtf8(str::Utf8Error),
    DeserializeInt(num::ParseIntError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl convert::From<str::Utf8Error> for Error {
    fn from(error: str::Utf8Error) -> Error {
        Error::DeserializeUtf8(error)
    }
}

impl convert::From<num::ParseIntError> for Error {
    fn from(error: num::ParseIntError) -> Error {
        Error::DeserializeInt(error)
    }
}

impl convert::From<url::ParseError> for Error {
    fn from(error: url::ParseError) -> Error {
        Error::UrlParse(error)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::BadUrl => "Wrong url!",
            Error::NotFound => "RTSP header not found!",
            Error::Serialize => "RTSP message deserialization error!",
            Error::Deserialize => "RTSP message deserialization error!",
            Error::StartString => "Start string malformed!",
            Error::StructureInit => "Structure init failed!",
            Error::CodeNotAllowed => "This code not supported!",
            Error::NotImplemented => "Not implemented!",
            Error::HeaderNotAllowed => "This header not supported!",
            Error::UrlParse(e) => e.description(),
            Error::DeserializeUtf8(e) => e.description(),
            Error::DeserializeInt(e) => e.description(),
        }
    }
}