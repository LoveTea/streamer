use std::ops::Sub;
use std::time::{ Instant, Duration, SystemTime, UNIX_EPOCH };

use rtsp_utils::*;

pub const RTCP_SENDER_REPORT_SEND_INTERVAL_SEC: i32 = 1;

pub struct RtcpContext {
    bytes_count: u32,
    packet_count: u32,
    pub client_port: u16,
    is_first_report: bool,
    ntp_time_sync: Instant,
    last_rtp_timestamp: u32,
    unix_time_start: Duration,
    last_report_time: Instant,

}

impl RtcpContext {
    pub fn new() -> Self {
        let time = SystemTime::now();
        let unix_time = time.duration_since(UNIX_EPOCH).unwrap();

        RtcpContext {
            client_port: 0,
            bytes_count: 0,
            packet_count: 0,
            last_rtp_timestamp: 0,
            is_first_report: true,
            unix_time_start: unix_time,
            ntp_time_sync: Instant::now(),
            last_report_time: Instant::now(),
        }
    }

    pub fn increment_packet_count(&mut self) {
        self.packet_count += 1;
    }

    pub fn add_bytes_count(&mut self, bytes: u32) {
        self.bytes_count += bytes;
    }

    pub fn set_rtp_timestamp(&mut self, timestamp: u32) {
        self.last_rtp_timestamp = timestamp;
    }

    pub fn get_packet_count(&self) -> u32 {
        self.packet_count
    }

    pub fn get_bytes_count(&self) -> u32 {
        self.bytes_count
    }

    pub fn is_first_report(&self) -> bool {
        self.is_first_report
    }

    pub fn get_rtp_timestamp(&self) -> u32 {
        self.last_rtp_timestamp
    }

    pub fn start_stream(&mut self) {
        self.ntp_time_sync = Instant::now();
    }

    pub fn report_send(&mut self) {
        self.is_first_report = false;
        self.last_report_time = Instant::now();
    }

    pub fn calculate_current_rtp_time(&self, frequency: f32) -> u32 {
        let ms_elapsed = self.ntp_time_sync.elapsed().as_secs() * 1000
            + self.ntp_time_sync.elapsed().subsec_millis() as u64;
        ((ms_elapsed as f32 * frequency) / 1000 as f32) as u32
    }

    pub fn is_time_to_send(&self) -> bool {
        if self.last_report_time.elapsed().as_secs() >= RTCP_SENDER_REPORT_SEND_INTERVAL_SEC as u64 {
            true
        } else {
            false
        }
    }

    pub fn sync_ntp(&mut self, time_offset_mcs: u64) -> Result<()> {
        let offset = Duration::new(time_offset_mcs / 1000000, (time_offset_mcs % 1000000) as u32);
        self.ntp_time_sync = Instant::now().sub(offset);

        Ok(())
    }

    pub fn get_ntp_time(&self) -> (u32, u32) {
        let ntp_time = self.unix_time_start + self.ntp_time_sync.elapsed() + Duration::new(2208988800, 0);
        (
            ntp_time.as_secs() as u32,
            ntp_time.subsec_nanos(),
        )
    }
}