use rtsp_utils::*;
use rtp_serializer::PROTOCOL_VERSION;

pub const RTCP_SENDER_REPORT_TYPE: u8 = 200;
pub const RTCP_SENDER_REPORT_SIZE: usize = 28;
pub const RTCP_SENDER_REPORT_LENGTH: u16 = 6;

pub struct RtcpSenderReport {
    pub version: u8,
    pub padding: u8,
    pub rc: u8,
    pub report_type: u8,
    pub length: u16,
    pub ssrc: u32,
    pub ntp_timestamp_most: u32,
    pub ntp_timestamp_least: u32,
    pub rtp_timestamp: u32,
    pub packet_count: u32,
    pub bytes_count: u32,
}

impl RtcpSenderReport {
    pub fn new() -> Self {
        RtcpSenderReport {
            version: PROTOCOL_VERSION,
            padding: 0,
            rc: 0,
            report_type: RTCP_SENDER_REPORT_TYPE,
            length: RTCP_SENDER_REPORT_LENGTH,
            ssrc: 0,
            ntp_timestamp_most: 0,
            ntp_timestamp_least: 0,
            rtp_timestamp: 0,
            packet_count: 0,
            bytes_count: 0,
        }
    }

    pub fn serialize(&mut self) -> Result<Vec<u8>> {
        if self.version != PROTOCOL_VERSION {
            error!("RPT packet version mismatch!");
            return Err(Error::HeaderNotAllowed);
        }

        let mut report = vec![0u8; RTCP_SENDER_REPORT_SIZE];
        report[0] |= self.version << 6;
        report[0] |= self.padding << 5;
        report[0] |= self.rc;
        report[1] |= self.report_type;
        report[2] |= (self.length >> 8) as u8;
        report[3] |= self.length as u8;
        report[4] |= (self.ssrc >> 24) as u8;
        report[5] |= (self.ssrc >> 16) as u8;
        report[6] |= (self.ssrc >> 8) as u8;
        report[7] |= self.ssrc as u8;
        report[8] |= (self.ntp_timestamp_most >> 24) as u8;
        report[9] |= (self.ntp_timestamp_most >> 16) as u8;
        report[10] |= (self.ntp_timestamp_most >> 8) as u8;
        report[11] |= self.ntp_timestamp_most as u8;
        report[12] |= (self.ntp_timestamp_least >> 24) as u8;
        report[13] |= (self.ntp_timestamp_least >> 16) as u8;
        report[14] |= (self.ntp_timestamp_least >> 8) as u8;
        report[15] |= self.ntp_timestamp_least as u8;
        report[16] |= (self.rtp_timestamp >> 24) as u8;
        report[17] |= (self.rtp_timestamp >> 16) as u8;
        report[18] |= (self.rtp_timestamp >> 8) as u8;
        report[19] |= self.rtp_timestamp as u8;
        report[20] |= (self.packet_count >> 24) as u8;
        report[21] |= (self.packet_count >> 16) as u8;
        report[22] |= (self.packet_count >> 8) as u8;
        report[23] |= self.packet_count as u8;
        report[24] |= (self.bytes_count >> 24) as u8;
        report[25] |= (self.bytes_count >> 16) as u8;
        report[26] |= (self.bytes_count >> 8) as u8;
        report[27] |= self.bytes_count as u8;

        Ok(report)
    }
}