use rtsp_utils::*;

pub const PACKET_HEADER_LENGTH: usize = 12;
pub const PROTOCOL_VERSION: u8 = 2;

pub struct RtpPacket {
    pub version: u8,
    pub padding: u8,
    pub extension: u8,
    pub cc: u8,
    pub marker: u8,
    pub payload_type: u8,
    pub sequence_number: u16,
    pub timestamp: u32,
    pub ssrc: u32,
    pub payload: Vec<u8>,
}

impl RtpPacket {
    pub fn new() -> Self {
        RtpPacket {
            version: PROTOCOL_VERSION,
            padding: 0,
            extension: 0,
            cc: 0,
            marker: 0,
            payload_type: 0,
            sequence_number: 0,
            timestamp: 0,
            ssrc: 0,
            payload: Vec::new(),
        }
    }

    pub fn serialize(&mut self) -> Result<Vec<u8>> {
        if self.version != PROTOCOL_VERSION {
            error!("RPT packet version mismatch!");
            return Err(Error::HeaderNotAllowed);
        }

        let mut packet = vec![0u8; PACKET_HEADER_LENGTH];
        packet[0] |= self.version << 6;
        packet[0] |= self.padding << 5;
        packet[0] |= self.extension << 4;
        packet[0] |= self.cc;
        packet[1] |= self.marker << 7;
        packet[1] |= self.payload_type;
        packet[2] |= (self.sequence_number >> 8) as u8;
        packet[3] |= (self.sequence_number) as u8;
        packet[4] |= (self.timestamp >> 24) as u8;
        packet[5] |= (self.timestamp >> 16) as u8;
        packet[6] |= (self.timestamp >> 8) as u8;
        packet[7] |= (self.timestamp) as u8;
        packet[8] |= (self.ssrc >> 24) as u8;
        packet[9] |= (self.ssrc >> 16) as u8;
        packet[10] |= (self.ssrc >> 8) as u8;
        packet[11] |= (self.ssrc) as u8;
        packet.append(&mut self.payload);

        Ok(packet)
    }
}