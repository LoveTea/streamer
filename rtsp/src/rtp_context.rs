use rand::random;
use std::ops::Sub;
use std::time::{ Instant, Duration };

pub const MAX_RTP_PAYLOAD_SIZE: usize = 65000;

pub const SEND_FRAME_OFFSET_MS: i64 = 300;

#[derive(Debug, Clone, PartialEq)]
pub enum RtpState { PLAY, PAUSE }

pub struct RtpContext {
    seq: u16,
    ssrc: u32,
    state: RtpState,
    pub client_port: u16,
    current_time: Instant,
    is_stream_start: bool,
}

impl RtpContext {
    pub fn new() -> Self {
        RtpContext {
            seq: 1,
            client_port: 0,
            ssrc: random::<u32>(),
            state: RtpState::PLAY,
            current_time: Instant::now(),
            is_stream_start: false,
        }
    }

    pub fn is_steam_start(&self) -> bool {
        self.is_stream_start
    }

    pub fn get_sync_source(&self) -> u32 {
        self.ssrc
    }

    pub fn get_next_sequence(&mut self) -> u16 {
        let seq = self.seq;
        self.seq += 1;
        seq
    }

    pub fn start_stream(&mut self) {
        self.is_stream_start = true;
        self.current_time = Instant::now();
    }

    pub fn get_stream_elapsed_ms(&self) -> i64 {
        (self.current_time.elapsed().as_secs() * 1000) as i64 + self.current_time.elapsed().subsec_millis() as i64
    }

    pub fn get_state(&self) -> RtpState {
        self.state.clone()
    }

    pub fn play(&mut self, seek_offset_mcs: u64) {
        self.state = RtpState::PLAY;

        if seek_offset_mcs == 0 {
            return;
        }

        let offset = Duration::new(seek_offset_mcs / 1000000, (seek_offset_mcs % 1000000) as u32);
        self.current_time = Instant::now().sub(offset);
    }

    pub fn pause(&mut self) {
        self.state = RtpState::PAUSE;
    }
}