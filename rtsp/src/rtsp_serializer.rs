use std::str;
use url::Url;
use std::option::Option;
use std::collections::HashMap;

use rtsp_utils::*;

pub fn deserialize(data: &[u8], processed: &mut usize) -> Result<RtspMessage> {
    let mut string_buffer = match str::from_utf8(data) {
        Ok(buf) => buf.to_string(),
        Err(e) => {
            if e.valid_up_to() == 0 {
                error!("Not valid string!");
                return Err(Error::Deserialize);
            }

            str::from_utf8(data[0..e.valid_up_to()].as_ref()).unwrap().to_string()
        }
    };

    let mut message = RtspMessage::new();
    let mut index = 0;
    loop {
        if string_buffer.is_empty() {
            break;
        }

        let pos = string_buffer.find(NEW_LINE);
        let line: String;
        if pos.is_some() {
            line = string_buffer[..*pos.as_ref().unwrap()].to_string();
            string_buffer = string_buffer[pos.unwrap() + NEW_LINE.len()..].to_string();
            *processed += NEW_LINE.len();
        } else {
            line = string_buffer.clone();
            string_buffer.clear();
        }

        *processed += line.len();
        if index == 0 {
            if line.to_uppercase().starts_with("RTSP") {
                parse_response_start_string(&mut message, line.as_ref());
            } else {
                parse_request_start_string(&mut message, line.as_ref());
            }
        } else {
            if line.is_empty() {
                break;
            }

            let position = line.find(':').unwrap_or_default();
            let (key, value) = (line[0..position].to_lowercase().trim().to_string(),
                line[position + 1..].trim().to_string());
            message.headers.insert(key, value);
        }

        index += 1;
    }

    Ok(message)
}

fn parse_request_start_string(message: &mut RtspMessage, start_string: &str) {
    message.message_type = MessageType::REQUEST;
    let tokens = start_string.split(' ').collect::<Vec<&str>>();
    if tokens.len() != 3 {
        debug!("Request start string parse error!");
        message.command = "".to_string();
        message.url = "".to_string();
        message.version = "".to_string();
    } else {
        message.command = tokens[0].to_string();
        message.url = tokens[1].to_lowercase().to_string();
        message.version = tokens[2].to_uppercase().to_string();
    }
}

fn parse_response_start_string(message: &mut RtspMessage, start_string: &str) {
    message.message_type = MessageType::RESPONSE;
    let tokens = start_string.split(' ').collect::<Vec<&str>>();

    if tokens.len() < 3 {
        debug!("Response start string parse error!");
        message.version = "".to_string();
        message.code = 0;
        message.code_description = "".to_string();
    } else {
        message.version = tokens[0].to_uppercase().to_string();
        message.code = tokens[1].to_string().parse::<i32>().unwrap_or(0);
        for token in tokens[2..].iter() {
            message.code_description.push_str(token);
            message.code_description.push(' ');
        }
        message.code_description = message.code_description.trim_right().to_string();
    }
}

pub fn serialize(message: &RtspMessage) -> Result<String> {
    if message.version.is_empty() || message.code == 0 || message.code_description.is_empty() {
        error!("Invalid start string");
        return Err(Error::Serialize);
    }

    let mut result = String::new();
    result.push_str(message.version.as_ref()); result.push(' ');
    result.push_str(message.code.to_string().as_ref()); result.push(' ');
    result.push_str(message.code_description.as_ref());
    result.push_str(NEW_LINE);

    let delimiter = ": ".to_string();
    for (key, value) in &message.headers {
        result.push_str(key.as_ref());
        result.push_str(delimiter.as_ref());
        result.push_str(value.as_ref());
        result.push_str(NEW_LINE);
    }
    result.push_str(NEW_LINE);
    if !message.body.is_empty() {
        result.push_str(message.body.as_ref());
    }

    Ok(result)
}

pub struct TransportHeader {
    _transport: String,
    _cast: String,
    client_rtp_port: u16,
    client_rtcp_port: u16,
}

impl TransportHeader {
    pub fn get_client_rtp_port(&self) -> u16 {
        self.client_rtp_port
    }

    pub fn get_client_rtcp_port(&self) -> u16 {
        self.client_rtcp_port
    }
}

#[derive(Debug, PartialEq)]
pub enum MessageType {
    REQUEST,
    RESPONSE
}

#[derive(Debug)]
pub struct RtspMessage {
    message_type: MessageType,
    command: String,
    url: String,
    version: String,
    code: i32,
    code_description: String,
    headers: HashMap<String, String>,
    body: String,
}

impl RtspMessage {
    pub fn new() -> Self {
        RtspMessage {
            message_type: MessageType::RESPONSE,
            command: String::new(),
            url: String::new(),
            version: "RTSP/1.0".to_string(),
            code: 0,
            code_description: String::new(),
            headers: HashMap::new(),
            body: String::new(),
        }
    }

    pub fn validate(&self) -> Result<()> {
        if !ALLOWED_VERSIONS.contains(&self.version.as_ref()) {
            return Err(Error::StartString);
        }

        if self.message_type == MessageType::REQUEST {
            if !self.command.is_empty() {
                if !ALLOWED_COMMANDS.contains(&self.command.as_ref()) {
                    if COMMANDS.contains(&self.command.as_ref()) {
                        return Err(Error::NotImplemented);
                    } else {
                        return Err(Error::StartString);
                    }
                }
            }

            let url = Url::parse(&self.url)?;
            debug!("URL {}", url);
            if url.scheme() != "rtsp" {
                return Err(Error::BadUrl);
            }
        } else {
            if self.code < 100 || self.code > 600 {
                return Err(Error::StartString);
            }

            if self.code_description.is_empty() {
                return Err(Error::StartString);
            }
        }

        Ok(())
    }

    pub fn set_code(&mut self, code: i32) -> Result<()> {
        if !ALLOWED_CODES.contains(&code) {
            error!("Code not allowed!");
            return Err(Error::CodeNotAllowed);
        }

        self.code = code;

        Ok(())
    }

    pub fn set_code_description(&mut self, description: &str) {
        self.code_description = description.to_string();
    }

    pub fn set_header(&mut self, key: &str, value: &str) -> Result<()> {
        if !ALLOWED_HEADERS.contains(&key) {
            error!("Header not allowed!");
            return Err(Error::HeaderNotAllowed);
        }

        self.headers.insert(key.to_string(), value.to_string());

        Ok(())
    }

    pub fn get_url(&self) -> String {
        self.url.clone()
    }

    pub fn set_body(&mut self, body: &str) {
        self.body = body.to_string();
    }

    pub fn get_header(&self, key: &str) -> Option<&String> {
        self.headers.get(&key.to_lowercase())
    }

    pub fn get_type(&self) -> &MessageType {
        &self.message_type
    }

    pub fn get_command(&self) -> &str {
        self.command.as_ref()
    }

    pub fn parse_range_header(&self) -> Result<(f32, f32)> {
        let range_string = self.headers.get("range").ok_or(Error::NotFound)?;
        let splitted_range = range_string.split("=").collect::<Vec<&str>>();
        if splitted_range.len() < 2 {
            return Err(Error::NotFound);
        }
        if !splitted_range[0].starts_with("npt") {
            error!("Invalid seek method!");
            return Err(Error::NotFound);
        }

        let splitted_values = splitted_range[1].trim().split("-").collect::<Vec<&str>>();
        if splitted_values.is_empty() {
            error!("Invalid range value!");
            return Err(Error::NotFound);
        }

        let start = splitted_values[0].parse::<f32>();
        if start.is_err() {
            error!("Start range parse failed!");
            return Err(Error::NotFound);
        }

        let end = {
            if splitted_values.len() > 1 {
                let value = splitted_values[1].parse::<f32>();
                if value.is_err() {
                    Ok(-1f32)
                } else {
                    value
                }
            } else {
                Ok(-1f32)
            }
        };

        Ok((start.unwrap(), end.unwrap()))
    }

    pub fn parse_transport_header(&self) -> Result<TransportHeader> {
        let transport_string = self.get_header("Transport").ok_or(Error::NotFound)?;
        let tokens = transport_string.split(";").collect::<Vec<&str>>();
        if tokens.len() < 3 {
            error!("Transport header too short!");
            return Err(Error::Deserialize);
        }

        let mut ports = Vec::<&str>::new();
        for i in 0..tokens.len() {
            if tokens[i].starts_with("client_port") {
                let client_port_vec = tokens[i].split("=").collect::<Vec<&str>>();
                if client_port_vec.len() < 2 {
                    error!("Client ports not valid!");
                    return Err(Error::Deserialize);
                }
                let ports_string = client_port_vec[1];
                ports = ports_string.split("-").collect::<Vec<&str>>();
                if ports.len() < 2 {
                    error!("Client ports not valid!");
                    return Err(Error::Deserialize);
                }
            }
        }

        let header = TransportHeader {
            _transport: tokens[0].to_string(),
            _cast: tokens[1].to_string(),
            client_rtp_port: ports[0].to_string().parse::<u16>()?,
            client_rtcp_port: ports[1].to_string().parse::<u16>()?,
        };

        Ok(header)
    }
}