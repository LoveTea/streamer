use ini::ini;
use std::num;
use std::fmt;
use std::error;
use std::result;
use std::convert;

use libevent::event_utils;

pub const UDP_ADDRESS_RANGE: (u16, u16) = (11000, 12001);

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    UdpSocket,
    FrameToBig,
    ParseError,
    OptionNotFound,
    SettingsNotFound,
    UnknownSyncEvent,
    SettingsParse(ini::Error),
    IntParse(num::ParseIntError),
    LibEvent(event_utils::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl convert::From<ini::Error> for Error {
    fn from(error: ini::Error) -> Error {
        Error::SettingsParse(error)
    }
}

impl convert::From<num::ParseIntError> for Error {
    fn from(error: num::ParseIntError) -> Error {
        Error::IntParse(error)
    }
}

impl convert::From<event_utils::Error> for Error {
    fn from(error: event_utils::Error) -> Error {
        Error::LibEvent(error)
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            Error::UdpSocket => "UDP socket initialization failed!",
            Error::FrameToBig => "UDP socket initialization failed!",
            Error::ParseError => "Settings parse error!",
            Error::OptionNotFound => "Option value not found!",
            Error::SettingsNotFound => "Property not found!",
            Error::UnknownSyncEvent => "Unknown sync event!",
            Error::SettingsParse(e) => e.description(),
            Error::IntParse(e) => e.description(),
            Error::LibEvent(e) => e.description(),
        }
    }
}