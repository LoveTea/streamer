extern crate simple_logger;

extern crate streamer;

use std::env;

use streamer::server;

fn main() {
    simple_logger::init().expect("Log init failed!");

    let args:Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Server settings not set!");
    }

    let setting_path = args.get(1).unwrap().to_string();
    let mut server = server::Server::new(setting_path.as_ref()).unwrap();
    server.start().expect("Server start!");
}
