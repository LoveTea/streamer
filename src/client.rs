use base64;
use url::Url;
use std::str;
use std::error;
use std::result;
use std::vec::Vec;
use chrono::Local;
use std::boxed::Box;
use std::time::Instant;
use std::sync::mpsc::Receiver;

use utils::*;
use server::*;
use rtsp::rtsp_utils;
use rtsp::rtp_context::*;
use rtsp::rtcp_context::*;
use rtsp::rtp_serializer::*;
use rtsp::rtsp_serializer::*;
use rtsp::rtcp_serializer::*;
use libevent::event_udp::UdpEvent;
use streamer_core::streamer_core::*;
use libevent::event_timer::EventTimer;
use libevent::event_buffer::BufferEvent;

const AUDIO_FIX_FREQUENCY: f32 = 90000f32;

#[derive(Debug)]
pub enum Event {
    RTSPRead,
    RTPRead,
    RTCPRead,
    Timeout,
    Terminate,
}

pub struct Client {
    is_terminate: bool,
    session_number: u64,
    rtsp_buffer: Vec<u8>,
    rtsp_bev: BufferEvent,
    client_number: Box<u64>,
    receiver: Receiver<Event>,
    streamer: Option<Streamer>,
    pub(crate) timer: EventTimer,
    video_rtp_context: RtpContext,
    audio_rtp_context: RtpContext,
    video_rtcp_context: RtcpContext,
    audio_rtcp_context: RtcpContext,
    audio_rtp_event: Option<Box<UdpEvent>>,
    video_rtp_event: Option<Box<UdpEvent>>,
    audio_rtcp_event: Option<Box<UdpEvent>>,
    video_rtcp_event: Option<Box<UdpEvent>>,
}

impl Client {
    pub fn new(client_number: Box<u64>, session_number: u64, bev: BufferEvent, receiver: Receiver<Event>,
        timer: EventTimer) -> Self
    {
        debug!("Client()");

        Client {
            timer,
            receiver,
            rtsp_bev: bev,
            client_number,
            session_number,
            streamer: None,
            is_terminate: false,
            video_rtp_event: None,
            audio_rtp_event: None,
            video_rtcp_event: None,
            audio_rtcp_event: None,
            rtsp_buffer: Vec::new(),
            video_rtp_context: RtpContext::new(),
            audio_rtp_context: RtpContext::new(),
            video_rtcp_context: RtcpContext::new(),
            audio_rtcp_context: RtcpContext::new(),
        }
    }

    pub fn event_loop(&mut self) {
        loop {
            if self.is_terminate {
                return;
            }

            let message = self.receiver.recv();
            if message.is_err() {
                self.terminate();
            } else {
                let event = message.unwrap();
                debug!("New event {} {:?}", *self.client_number, event);
                match event {
                    Event::RTSPRead => self.read_rtsp(),
                    Event::RTPRead => self.read_rtp(),
                    Event::RTCPRead => self.read_rtcp(),
                    Event::Timeout => self.on_timeout(),
                    Event::Terminate => {
                        self.terminate();
                    },
                };
            }
        }
    }

    fn set_timer(&mut self, seconds: i32, microseconds: i32) -> result::Result<(), Box<error::Error>> {
        let server = { get_server() };
        server.sync_set_timer(self as &mut Client, seconds, microseconds)?;

        Ok(())
    }

    fn on_timeout(&mut self) {
        if self.video_rtp_context.get_state() == RtpState::PAUSE {
            return;
        }

        match self.rtp_send_frame() {
            Ok(_) => (),
            Err(e) => error!("Send frame failed! {}", e),
        }
    }

    fn read_rtsp(&mut self) {
        let length = self.rtsp_bev.bytes_available();
        let mut data= vec![0u8; length];
        match self.rtsp_bev.read(data.as_ptr() as *mut u8, length) {
            Ok(_) => (),
            Err(e) => {
                error!("Buffer event read failed! {}", e);
                return;
            },
        };
        self.rtsp_buffer.append(&mut data);

        self.parse_rtsp();
    }

    fn parse_rtsp(&mut self) {
        while self.rtsp_buffer.len() > 0 {
            {
                let origin = str::from_utf8(&self.rtsp_buffer);
                debug!("{} {:?}", self.rtsp_buffer.len(), self.rtsp_buffer);
                println!("ORIGIN {} {:?}", self.rtsp_buffer.len(), origin);
            }

            let mut processed: usize = 0;
            let message = match deserialize(&self.rtsp_buffer, &mut processed) {
                Ok(msg) => msg,
                Err(e) => {
                    error!("Message parse failed! {}", e);
                    self.rtsp_buffer.clear();
                    return;
                }
            };
            self.rtsp_buffer = self.rtsp_buffer[processed..].to_vec();

            println!("RTSP {:?}", message);

            match message.validate() {
                Ok(_) => (),
                Err(e) => {
                    error!("Client parse error! {}", e);
                    continue;
                }
            }

            println!("COMMAND {:?}", message.get_command());

            let response: result::Result<String, Box<error::Error>>;
            if *message.get_type() == MessageType::REQUEST {
                response = match message.get_command() {
                    "OPTIONS" => self.response_options(&message),
                    "DESCRIBE" => self.response_describe(&message),
                    "SETUP" => self.response_setup(&message),
                    "PLAY" => self.response_play(&message),
                    "PAUSE" => self.response_pause(&message),
                    "GET_PARAMETER" => self.response_get_parameter(&message),
                    "TEARDOWN" => self.response_teardown(&message),
                    _ => {
                        error!("Client unknown command!");
                        continue;
                    }
                };
            } else {
                continue;
            }

            let response_string = match response {
                Ok(response) => response,
                Err(e) => {
                    error!("Client RTSP response error! {}", e);
                    continue;
                }
            };

            println!("\n{}\n{:?}", response_string, response_string);

            self.rtsp_bev.write(response_string.as_ptr(), response_string.len()).unwrap_or_else(|e| {
                error!("Buffer event write failed! {}", e);
            });
        }
    }

    fn read_rtp(&mut self) {
        loop {
            let mut buffer = vec![0u8; 256];
            let read = self.video_rtp_event.as_mut().unwrap().read(&mut buffer);
            if read > 0 {
                buffer.resize(read as usize, 0);
                debug!("RTP READ {} {:?}", buffer.len(), buffer);
            } else {
                break;
            }
        }
    }

    fn read_rtcp(&mut self) {
        loop {
            let mut buffer = vec![0u8; 256];
            let read = self.video_rtcp_event.as_mut().unwrap().read(&mut buffer);
            if read > 0 {
                buffer.resize(read as usize, 0);
                debug!("RTCP READ {} {:?}", buffer.len(), buffer);
            } else {
                break;
            }
        }
    }

    fn response_options(&self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let mut commands = String::new();
        for command in rtsp_utils::ALLOWED_COMMANDS.iter() {
            commands.push_str(format!("{}, ", command).as_ref());
        }
        commands = commands.trim_right_matches(", ").to_string();

        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Public", commands.as_ref())?;

        let as_string = serialize(&response)?;

        Ok(as_string)
    }

    fn response_describe(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let server = get_server();
        let url = Url::parse(&message.get_url())?;
        let file_path = server.get_file_by_path(url.path());
        self.streamer = match Streamer::new(&file_path) {
            Ok(streamer) => Some(streamer),
            Err(e) => {
                error!("Video streamer open failed! {}", e);
                self.terminate();
                return Err(Box::new(e));
            },
        };

        let (video_frequency, duration, version, profile, compatibility, level,
            sps, pps, width, height, frame_rate) = {
            let streamer = self.streamer.as_mut().unwrap();
            (
                streamer.get_video_frequency(),
                streamer.get_duration(),
                streamer.get_version(),
                streamer.get_profile(),
                streamer.get_compatibility(),
                streamer.get_level(),
                streamer.get_sps(),
                streamer.get_pps(),
                streamer.get_width(),
                streamer.get_height(),
                streamer.get_frame_rate(),
            )
        };
        if sps.len() == 0 || pps.len() == 0 {
            error!("Stream parameters not found!");
            self.terminate();
            return Ok(String::new());
        }

        let content = format!("v=0\r\ns=Super server session\r\no=SuperServer 1 1 IN IP4 {} \
            \r\na=range:npt=0.000-{:.3}\r\na=control=*\r\nm=audio 0 RTP/AVP 14\r\na=rtpmap:14 mpa/{}/2 \
            \r\na=control:ctrl=1\r\nm=video 0 RTP/AVP 97\r\na=rtpmap:97 H264/{} \
            \r\na=fmtp:97 packetization-mode={};profile-level-id={:02X}{:02X}{:02X};sprop-parameter-sets={},{} \
            \r\na=cliprect:0,0,{w},{h}\r\na=framerate:{}\r\na=framesize:97 {h}-{w}\r\na=control:ctrl=2",
            get_server().get_host(), (duration as f32 / video_frequency as f32), AUDIO_FIX_FREQUENCY,
            video_frequency, version, profile, compatibility, level, base64::encode(&sps),
            base64::encode(&pps), frame_rate, h=height, w=width);

        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Session", self.session_number.to_string().as_ref())?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Content-Type", "application/sdp")?;
        response.set_header("Content-Length", content.len().to_string().as_ref())?;
        response.set_body(content.as_ref());

        let as_string = serialize(&response)?;

        Ok(as_string)
    }

    fn response_setup(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let url = message.get_url();
        let (client_rtp_port, client_rtcp_port, server_rtp_port, server_rtcp_port, ssrc) = {
            if url.ends_with("ctrl=1") {
                self.audio_setup(message)?
            } else {
                self.video_setup(message)?
            }
        };

        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Session", self.session_number.to_string().as_ref())?;
        response.set_header("Accept-Ranges", "npt")?;
        response.set_header("Transport",
            format!("RTP/AVP;unicast;client_port={}-{};server_port={}-{};source={};ssrc={}",
            client_rtp_port, client_rtcp_port, server_rtp_port, server_rtcp_port, get_server().get_host(), ssrc).as_ref())?;

        let as_string = serialize(&response)?;

        Ok(as_string)
    }

    fn response_play(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let (start, end, audio_seq, audio_pts, video_seq, video_pts) = {
            let result = self.stream_seek(message);
            if result.is_ok() {
                result.unwrap()
            } else {
                let (duration, audio_pts, video_pts) = {
                    let streamer = self.streamer.as_mut().ok_or(Error::OptionNotFound)?;
                    (
                        streamer.get_duration() as f32 / streamer.get_video_frequency() as f32,
                        streamer.get_next_audio_pts().unwrap_or(self.audio_rtcp_context.get_rtp_timestamp() as i64),
                        streamer.get_next_video_pts().unwrap_or(self.video_rtcp_context.get_rtp_timestamp() as i64),
                    )
                };

                (
                    self.video_rtp_context.get_stream_elapsed_ms() as f32 / 1000f32,
                    duration,
                    self.audio_rtcp_context.get_packet_count() + 1,
                    audio_pts as u32,
                    self.video_rtcp_context.get_packet_count() + 1,
                    video_pts as u32,
                )
            }
        };

        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Session", self.session_number.to_string().as_ref())?;
        response.set_header("Seek-Style", "CoRAP")?;
        response.set_header("Range", format!("npt={:.3}-{:.3}", start, end).as_ref())?;
        response.set_header("RTP-Info",
            format!("url=rtsp://{host}{};seq={};rtptime={},url=rtsp://{host}{};seq={};rtptime={}",
            "/ctrl=1", audio_seq, audio_pts, "/ctrl=2", video_seq, video_pts, host=get_server().get_host()).as_ref())?;

        let as_string = serialize(&response)?;

        if !self.video_rtp_context.is_steam_start() || !self.audio_rtp_context.is_steam_start() {
            self.set_timer(0, 500000)?;
        }

        Ok(as_string)
    }

    fn response_pause(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Session", self.session_number.to_string().as_ref())?;

        let as_string = serialize(&response)?;

        self.video_rtp_context.pause();

        Ok(as_string)
    }

    fn response_get_parameter(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("Session", self.session_number.to_string().as_ref())?;

        let as_string = serialize(&response)?;

        Ok(as_string)
    }

    fn response_teardown(&mut self, message: &RtspMessage) -> result::Result<String, Box<error::Error>> {
        let mut response = RtspMessage::new();
        response.set_code(200)?;
        response.set_code_description("OK");
        response.set_header("Server", "Super Server")?;
        response.set_header("Cache-Control", "no-cache")?;
        response.set_header("CSeq", message.get_header("CSeq").unwrap_or(&"0".to_string()))?;
        response.set_header("Date", Local::now().to_rfc2822().as_ref())?;

        let as_string = serialize(&response)?;

        self.terminate();

        Ok(as_string)
    }

    fn rtp_send_frame(&mut self) -> result::Result<(), Box<error::Error>> {
        let benchmark = Instant::now();

        let mut timeout = self.streamer.as_mut().ok_or(Error::OptionNotFound)?.get_send_timeout_ms();

        if self.audio_rtcp_context.is_first_report() && self.video_rtcp_context.is_first_report() {
            self.video_rtp_context.start_stream();
            self.audio_rtp_context.start_stream();
            self.video_rtcp_context.start_stream();
            self.audio_rtcp_context.start_stream();

            self.rtcp_send_audio_report()?;
            self.rtcp_send_video_report()?;
        } else {
            if self.audio_rtcp_context.is_time_to_send() {
                self.rtcp_send_audio_report()?;
            }
            if self.video_rtcp_context.is_time_to_send() {
                self.rtcp_send_video_report()?;
            }
        }

        loop {
            let (frequency, (nal, pts)) = {
                let streamer = self.streamer.as_mut().ok_or(Error::OptionNotFound)?;
                let frequency = streamer.get_video_frequency();
                let current_pts = streamer.get_next_video_pts()? as i64 * 1000 / frequency as i64;
                if current_pts - SEND_FRAME_OFFSET_MS > self.video_rtp_context.get_stream_elapsed_ms() {
                    break;
                }

                (
                    frequency,
                    streamer.get_video_nal()?,
                )
            };
            let stream_elapsed = self.video_rtp_context.get_stream_elapsed_ms();

            self.send_video_nal(nal, pts as u32)?;

            let pts_as_ms = pts * 1000 / frequency as i64;
            info!("VIDEO FRAME SENT {} {} {}", pts, pts_as_ms - SEND_FRAME_OFFSET_MS, stream_elapsed);
            if pts_as_ms + SEND_FRAME_OFFSET_MS > stream_elapsed {
                break;
            }
        }

        loop {
            let (frequency, (nal, pts)) = {
                let streamer = self.streamer.as_mut().ok_or(Error::OptionNotFound)?;
                let frequency = streamer.get_audio_frequency();
                let current_pts = streamer.get_next_audio_pts()? as i64 * 1000 / frequency as i64;
                if current_pts - SEND_FRAME_OFFSET_MS > self.audio_rtp_context.get_stream_elapsed_ms() {
                    break;
                }

                (
                    frequency,
                    streamer.get_audio_nal()?,
                )
            };
            let stream_elapsed = self.audio_rtp_context.get_stream_elapsed_ms();

            self.send_audio_nal(nal, pts as u32)?;

            let pts_as_ms = pts * 1000 / frequency as i64;
            info!("AUDIO FRAME SENT {} {} {}", pts, pts_as_ms - SEND_FRAME_OFFSET_MS, stream_elapsed);
            if pts_as_ms + SEND_FRAME_OFFSET_MS > stream_elapsed {
                break;
            }
        }

        timeout -= benchmark.elapsed().subsec_micros() as i32;
        timeout = if timeout > 0 { timeout } else { 0 };
        self.set_timer(0, timeout)?;

        Ok(())
    }

    fn send_video_nal(&mut self, nal: Vec<u8>, pts: u32) -> result::Result<(), Box<error::Error>> {
        let host = self.rtsp_bev.get_host().to_string();
        let port = self.video_rtp_context.client_port;
        let mut seq = self.video_rtp_context.get_next_sequence();
        let ssrc = self.video_rtp_context.get_sync_source();
        let nal_size = nal.len();

        if nal.len() <= MAX_RTP_PAYLOAD_SIZE {
            let mut packet = RtpPacket::new();
            packet.marker = 1;
            packet.sequence_number = seq;
            packet.timestamp = pts as u32;
            packet.ssrc = ssrc;
            packet.payload_type = 97;
            packet.payload = nal;
            self.video_rtp_event.as_mut().ok_or(Error::OptionNotFound)?.write(host.as_ref(), port, &packet.serialize()?)?;
        } else {
            let mut already_send = 0;
            let original_nal_header = nal[0];
            loop {
                let mut last_fragment = false;
                let mut current_payload_size = nal.len() - already_send;
                if current_payload_size > MAX_RTP_PAYLOAD_SIZE {
                    current_payload_size = MAX_RTP_PAYLOAD_SIZE;
                }

                if already_send + current_payload_size >= nal.len() {
                    last_fragment = true
                }

                let fu_indicator = original_nal_header & 0xe0 | 28u8;
                let mut fu_header = original_nal_header & 0xdf;
                if already_send == 0 { fu_header |= 0x80; } else { fu_header &= 0x7f; }
                if last_fragment { fu_header |= 0x40; } else { fu_header &= 0xbf; }

                let mut fragment = vec![fu_indicator, fu_header];
                let begin_slice = already_send + if already_send == 0 { 1 } else { 0 };
                let end_slice = already_send + current_payload_size;
                let mut raw_fragment = nal[begin_slice..end_slice].to_vec();
                fragment.append(&mut raw_fragment);

                let mut packet = RtpPacket::new();
                packet.marker = 1;
                packet.sequence_number = seq;
                packet.timestamp = pts as u32;
                packet.ssrc = ssrc;
                packet.payload_type = 97;
                packet.payload = fragment;
                self.video_rtp_event.as_mut().ok_or(Error::OptionNotFound)?.write(host.as_ref(), port, &packet.serialize()?)?;

                already_send += current_payload_size;

                if last_fragment {
                    break;
                }

                seq = self.video_rtp_context.get_next_sequence();
            }
        }

        self.video_rtcp_context.set_rtp_timestamp(pts as u32);
        self.video_rtcp_context.increment_packet_count();
        self.video_rtcp_context.add_bytes_count(nal_size as u32);

        Ok(())
    }

    fn send_audio_nal(&mut self, mut nal: Vec<u8>, pts: u32) -> result::Result<(), Box<error::Error>> {
        let host = self.rtsp_bev.get_host().to_string();
        let port = self.audio_rtp_context.client_port;
        let seq = self.audio_rtp_context.get_next_sequence();
        let ssrc = self.audio_rtp_context.get_sync_source();
        let nal_size = nal.len();

        let frequency = self.streamer.as_ref().ok_or(Error::OptionNotFound)?.get_audio_frequency();
        let pts_90kh = (pts as f32 / frequency as f32 * AUDIO_FIX_FREQUENCY) as u32;

        let mut nal_with_header = vec![0u8; 4];
        nal_with_header.append(&mut nal);

        let mut packet = RtpPacket::new();
        packet.marker = 0;
        packet.sequence_number = seq;
        packet.timestamp = pts_90kh;
        packet.ssrc = ssrc;
        packet.payload_type = 14;
        packet.payload = nal_with_header;
        self.audio_rtp_event.as_mut().ok_or(Error::OptionNotFound)?.write(host.as_ref(), port, &packet.serialize()?)?;

        self.audio_rtcp_context.set_rtp_timestamp(pts_90kh);
        self.audio_rtcp_context.increment_packet_count();
        self.audio_rtcp_context.add_bytes_count(nal_size as u32);

        Ok(())
    }

    fn rtcp_send_video_report(&mut self) -> result::Result<(), Box<error::Error>> {
        let frequency = self.streamer.as_mut().ok_or(Error::OptionNotFound)?.get_video_frequency();
        let mut report = RtcpSenderReport::new();
        report.ssrc = self.video_rtp_context.get_sync_source();
        let (most, least) = self.video_rtcp_context.get_ntp_time();
        report.ntp_timestamp_most = most;
        report.ntp_timestamp_least = least;
//        report.rtp_timestamp = self.video_rtcp_context.get_rtp_timestamp();
        report.rtp_timestamp = self.video_rtcp_context.calculate_current_rtp_time(frequency as f32);
        report.packet_count = self.video_rtcp_context.get_packet_count();
        report.bytes_count = self.video_rtcp_context.get_bytes_count();

        let (host, port) = (self.rtsp_bev.get_host(), self.video_rtcp_context.client_port);
        self.video_rtcp_event.as_mut().ok_or(Error::OptionNotFound)?.write(host.as_ref(), port, &report.serialize()?)?;

        self.video_rtcp_context.report_send();

        Ok(())
    }

    fn rtcp_send_audio_report(&mut self) -> result::Result<(), Box<error::Error>> {
        let mut report = RtcpSenderReport::new();
        report.ssrc = self.audio_rtp_context.get_sync_source();
        let (most, least) = self.audio_rtcp_context.get_ntp_time();
        report.ntp_timestamp_most = most;
        report.ntp_timestamp_least = least;
//        report.rtp_timestamp = self.audio_rtcp_context.get_rtp_timestamp();
        report.rtp_timestamp = self.audio_rtcp_context.calculate_current_rtp_time(AUDIO_FIX_FREQUENCY);
        report.packet_count = self.audio_rtcp_context.get_packet_count();
        report.bytes_count = self.audio_rtcp_context.get_bytes_count();

        let (host, port) = (self.rtsp_bev.get_host(), self.audio_rtcp_context.client_port);
        self.audio_rtcp_event.as_mut().ok_or(Error::OptionNotFound)?.write(host.as_ref(), port, &report.serialize()?)?;

        self.audio_rtcp_context.report_send();

        Ok(())
    }

    fn video_setup(&mut self, message: &RtspMessage) -> result::Result<(u16, u16, u16, u16, u32), Box<error::Error>> {
        let server = get_server();
        let (rtp, rtcp) = server.open_udp_transport(&mut *self.client_number)?;
        self.video_rtp_event = Some(rtp);
        self.video_rtcp_event = Some(rtcp);

        let transport = message.parse_transport_header()?;
        self.video_rtp_context.client_port = transport.get_client_rtp_port();
        self.video_rtcp_context.client_port = transport.get_client_rtcp_port();

        Ok((
            self.video_rtp_context.client_port,
            self.video_rtcp_context.client_port,
            self.video_rtp_event.as_ref().ok_or(Error::OptionNotFound)?.get_port(),
            self.video_rtcp_event.as_ref().ok_or(Error::OptionNotFound)?.get_port(),
            self.video_rtp_context.get_sync_source(),
        ))
    }

    fn audio_setup(&mut self, message: &RtspMessage) -> result::Result<(u16, u16, u16, u16, u32), Box<error::Error>> {
        let server = get_server();
        let (rtp, rtcp) = server.open_udp_transport(&mut *self.client_number)?;
        self.audio_rtp_event = Some(rtp);
        self.audio_rtcp_event = Some(rtcp);

        let transport = message.parse_transport_header()?;
        self.audio_rtp_context.client_port = transport.get_client_rtp_port();
        self.audio_rtcp_context.client_port = transport.get_client_rtcp_port();

        Ok((
            self.audio_rtp_context.client_port,
            self.audio_rtcp_context.client_port,
            self.audio_rtp_event.as_ref().ok_or(Error::OptionNotFound)?.get_port(),
            self.audio_rtcp_event.as_ref().ok_or(Error::OptionNotFound)?.get_port(),
            self.audio_rtp_context.get_sync_source(),
        ))
    }

    fn stream_seek(&mut self, message: &RtspMessage)
        -> result::Result<(f32, f32, u32, u32, u32, u32), Box<error::Error>>
    {
        let mut video_seq = 1;
        let mut video_pts = 0;
        let mut audio_seq = 1;
        let mut audio_pts = 0;
        let mut start = 0f32;
        let (video_frequency, audio_frequency, end) = {
            let streamer = self.streamer.as_mut().ok_or(Error::OptionNotFound)?;
            (
                streamer.get_video_frequency() as f32,
                streamer.get_audio_frequency() as f32,
                streamer.get_duration() as f32 / streamer.get_video_frequency() as f32,
            )
        };

        let rtp_state = self.video_rtp_context.get_state();
        if rtp_state == RtpState::PAUSE {
            {
                self.set_timer(0, 0)?;
            }
            let streamer = self.streamer.as_mut().ok_or(Error::OptionNotFound)?;
            self.video_rtp_context.play(0);
            let (range_start, _) = message.parse_range_header().unwrap_or((-1f32, -1f32));
            if range_start as i32 != -1 {
                let seek = (range_start * video_frequency) as i64;
                streamer.seek(seek)?;
            }

            video_pts = streamer.get_next_video_pts()?;
            video_seq = self.video_rtcp_context.get_packet_count() + 1;

            audio_pts = (streamer.get_next_audio_pts()? as f32 / audio_frequency * AUDIO_FIX_FREQUENCY) as u32;
            audio_seq = self.audio_rtcp_context.get_packet_count() + 1;

            start = video_pts as f32 / video_frequency;

            let stream_offset = (start * 1000000f32) as u64;
            self.video_rtp_context.play(stream_offset);
            self.video_rtcp_context.sync_ntp(stream_offset)?;
            self.audio_rtp_context.play(stream_offset);
            self.audio_rtcp_context.sync_ntp(stream_offset)?;
            debug!("SEEK {}", range_start);
        }

        Ok((start, end, audio_seq, audio_pts as u32, video_seq, video_pts as u32))
    }

    pub fn get_client_number(&self) -> u64 {
        *self.client_number
    }

    pub fn get_timer(&mut self) -> &mut EventTimer {
        &mut self.timer
    }

    fn terminate(&mut self) {
        let server = get_server();
        server.disconnect_client(*self.client_number);
        self.is_terminate = true;
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        debug!("~Client()");
    }
}