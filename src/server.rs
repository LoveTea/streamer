use std::str;
use std::mem;
use ini::Ini;
use std::error;
use std::result;
use std::thread;
use rand::random;
use std::io::Cursor;
use std::boxed::Box;
use std::sync::Mutex;
use std::ptr::null_mut;
use libc::{ c_void, c_int };
use std::collections::HashMap;
use std::sync::mpsc::{ channel, Sender };
use byteorder::{ LittleEndian, ReadBytesExt, WriteBytesExt };

use utils::*;
use client::{Event, Client };
use global_utils::to_void_ptr;
use libevent::event_udp::UdpEvent;
use libevent::event_base::EventBase;
use libevent::event_timer::EventTimer;
use libevent::event_buffer::BufferEvent;
use libevent::event_connection_listener::EventConnectionListener;

pub(crate) static mut SERVER: *mut Server = null_mut();

pub(crate) fn get_server() -> &'static mut Server { unsafe { &mut *SERVER } }

fn on_connected(bev: BufferEvent, _context: *const c_void) {
    let server = unsafe { &mut *SERVER };
    server.new_connection(bev);
}

fn on_error(_context: *const c_void) {
    error!("Connection listener error!");
    panic!("SERVER ERROR!");
}

fn on_rtsp_read(_bev: &BufferEvent, context: *mut c_void) {
    let (server, client_number) = unsafe {
        (&mut *SERVER, *mem::transmute::<*const c_void, *mut u64>(context))
    };
    match server.clients.get_mut().unwrap().get_mut(&client_number) {
        Some(sender) => match sender.send(Event::RTSPRead) {
            Ok(_) => (),
            Err(e) => error!("RTSP. Client send event error! {}", e),
        },
        None => error!("RTSP. Client not found! {}", client_number),
    }
}

fn on_rtp_read(_socket: c_int, context: *mut c_void) {
    let (server, client_number) = unsafe {
        (&mut *SERVER, *mem::transmute::<*const c_void, *mut u64>(context))
    };
    match server.clients.get_mut().unwrap().get_mut(&client_number) {
        Some(sender) => match sender.send(Event::RTPRead) {
            Ok(_) => (),
            Err(e) => error!("RTP. Client send event error! {}", e),
        },
        None => error!("RTP. Client not found! {}", client_number),
    }
}

fn on_rtcp_read(_socket: c_int, context: *mut c_void) {
    let (server, client_number) = unsafe {
        (&mut *SERVER, *mem::transmute::<*const c_void, *mut u64>(context))
    };
    match server.clients.get_mut().unwrap().get_mut(&client_number) {
        Some(sender) => match sender.send(Event::RTCPRead) {
            Ok(_) => (),
            Err(e) => error!("RTCP. Client send event error! {}", e),
        },
        None => error!("RTCP. Client not found! {}", client_number),
    }
}

fn on_timer(_socket: c_int, context: *mut c_void) {
    let (server, client_number) = unsafe {
        (&mut *SERVER, *mem::transmute::<*const c_void, *mut u64>(context))
    };
    match server.clients.get_mut().unwrap().get_mut(&client_number) {
        Some(sender) => match sender.send(Event::Timeout) {
            Ok(_) => (),
            Err(e) => error!("Timeout. Client send event error! {}", e),
        },
        None => error!("Timeout. Client not found! {}", client_number),
    }
}

fn on_set_timer_read(_socket: c_int, _context: *mut c_void) {
    let server = unsafe { &mut *SERVER };
    match server.sync_event() {
        Ok(_) => (),
        Err(e) => error!("Set timer read failed! {}", e),
    };
}

fn on_disconnect(_bev: &BufferEvent, context: *mut c_void) {
    let (server, client_number) = unsafe {
        (&mut *SERVER, *mem::transmute::<*const c_void, *mut u64>(context))
    };
    server.disconnect_client(client_number);
}

#[derive(Debug, PartialEq)]
enum SyncEvent {
    Timer,
}

struct Settings {
    port: u16,
    host_for_client: String,
    sources: HashMap<String, String>,
}

pub struct Server {
    settings: Settings,
    event_base: EventBase,
    session_sequence: u64,
    set_timer_event: UdpEvent,
    client_number_sequence: u64,
    transport_socket_ports_sequence: u16,
    clients: Mutex<HashMap<u64, Sender<Event>>>,
    threads: HashMap<u64, Option<thread::JoinHandle<()>>>,
}

impl Server {
    pub fn new(settings_path: &str) -> Result<Self> {
        debug!("Server()");

        let mut port = 554u16;
        let mut host_for_client = String::new();
        let mut sources = HashMap::<String, String>::new();
        if !settings_path.is_empty() {
            let settings_ini = Ini::load_from_file(settings_path)?;
            let server_section = match settings_ini.section(Some("server")) {
                Some(props) => props,
                None => {
                    error!("Server section not found!");
                    return Err(Error::SettingsNotFound);
                },
            };
            host_for_client = server_section.get("host_for_client").unwrap_or(&"127.0.0.1".to_string()).to_string();
            port = server_section.get("port").unwrap_or(&"8554".to_string()).parse::<u16>()?;

            let source_count = match settings_ini.section(Some("videos")) {
                Some(count) => count.get("count").unwrap_or(&"0".to_string()).parse::<u32>()?,
                None => 0,
            };
            for i in 0..source_count {
                let source_section = match settings_ini.section(Some(format!("video{}", i))) {
                    Some(section) => section,
                    None => {
                        error!("Source section not found! {}", i);
                        return Err(Error::SettingsNotFound);
                    }
                };
                let key = source_section.get("path").ok_or(Error::ParseError)?.to_string();
                let value = source_section.get("file").ok_or(Error::ParseError)?.to_string();
                sources.insert(key, value);
            }
        }

        let settings = Settings {
            port,
            sources,
            host_for_client,
        };

        let event_base = EventBase::new()?;

        let set_timer_event = UdpEvent::new("127.0.0.1", 0)?;

        Ok(
            Server {
                settings,
                event_base,
                set_timer_event,
                threads: HashMap::new(),
                client_number_sequence: 0,
                session_sequence: random::<u64>(),
                clients: Mutex::new(HashMap::new()),
                transport_socket_ports_sequence: UDP_ADDRESS_RANGE.0,
            }
        )
    }

    pub fn start(&mut self) -> result::Result<(), Box<error::Error>> {
        unsafe { SERVER = self; };

        self.set_timer_event.set_callback(&mut self.event_base, on_set_timer_read, null_mut())?;

        let mut listener = EventConnectionListener::new(&mut self.event_base, "", self.settings.port)?;
        listener.set_callback(on_connected, on_error, null_mut());
        self.event_base.dispatch()?;

        Ok(())
    }

    pub fn open_udp_transport(&mut self, client_number: *mut u64) -> Result<(Box<UdpEvent>, Box<UdpEvent>)> {
        let (rtp_port, rtcp_port) = self.next_transport_ports();
        let mut rtp_socket = match UdpEvent::new("", rtp_port) {
            Ok(socket) => Box::new(socket),
            Err(e) => {
                error!("RTP socket initialization failed! {}", e);
                return Err(Error::UdpSocket);
            }
        };
        let mut rtcp_socket = match UdpEvent::new("", rtcp_port) {
            Ok(socket) => Box::new(socket),
            Err(e) => {
                error!("RTCP socket initialization failed! {}", e);
                return Err(Error::UdpSocket);
            }
        };

        rtp_socket.set_callback(&mut self.event_base, on_rtp_read, client_number as *mut c_void)?;
        rtcp_socket.set_callback(&mut self.event_base, on_rtcp_read, client_number as *mut c_void)?;

        Ok((rtp_socket, rtcp_socket))
    }

    pub fn new_connection(&mut self, bev: BufferEvent) {
        let mut client_number = Box::new(self.next_client_number());
        let client_number_copy = (*client_number).clone();
        debug!("Server new connection {}!", &mut *client_number);

        let (sender, receiver) = channel::<Event>();
        let thread = thread::spawn(move || {
            let mut bev_mut = bev;
            bev_mut.set_callback(on_rtsp_read, on_disconnect, to_void_ptr(&mut *client_number));
            bev_mut.enable();

            let mut timer = EventTimer::new();
            match timer.set_callback(bev_mut.get_event_base(), on_timer, to_void_ptr(&mut *client_number)) {
                Ok(_) => (),
                Err(e) => {
                    error!("Set timer callback error! {}", e);
                    return;
                },
            };

            let mut client = Client::new(client_number, get_server().next_session_number(),
                bev_mut, receiver, timer);
            client.event_loop();
        });

        self.threads.insert(client_number_copy.clone(), Some(thread));
        self.clients.get_mut().unwrap().insert(client_number_copy, sender);
    }

    fn next_client_number(&mut self) -> u64 {
        self.client_number_sequence += 1;
        self.client_number_sequence
    }

    fn next_transport_ports(&mut self) -> (u16, u16) {
        if self.transport_socket_ports_sequence > UDP_ADDRESS_RANGE.1 {
            self.transport_socket_ports_sequence = UDP_ADDRESS_RANGE.0;
        }

        let (rtp_port, rtcp_port) = (self.transport_socket_ports_sequence.clone(), self.transport_socket_ports_sequence + 1);
        self.transport_socket_ports_sequence += 2;
        (rtp_port, rtcp_port)
    }

    fn next_session_number(&mut self) -> u64 {
        let next_session = self.session_sequence;
        self.session_sequence += 1;
        next_session
    }

    pub fn sync_set_timer(&mut self, client: &mut Client, seconds: i32, microseconds: i32)
        -> result::Result<(), Box<error::Error>>
    {
        let mut buffer = vec![0u8; 0];
        buffer.write_u32::<LittleEndian>(SyncEvent::Timer as u32)?;
        buffer.write_u64::<LittleEndian>(client.get_client_number())?;
        buffer.write_u64::<LittleEndian>(to_void_ptr(client.get_timer()) as usize as u64)?;
        buffer.write_i32::<LittleEndian>(seconds)?;
        buffer.write_i32::<LittleEndian>(microseconds)?;

        let (host, port) = {
            (
                self.set_timer_event.get_host().to_string(),
                self.set_timer_event.get_port()
            )
        };

        self.set_timer_event.write(host.as_ref(), port, &buffer)?;

        Ok(())
    }

    fn sync_event(&mut self) -> result::Result<(), Box<error::Error>> {
        let buffer_length: usize = 64;
        loop {
            let mut buffer = vec![0u8; buffer_length];
            let read = self.set_timer_event.read(&mut buffer);
            if read < 0 {
                return Ok(());
            }

            let mut cursor = Cursor::new(buffer);
            let event = cursor.read_u32::<LittleEndian>()?;
            match event {
                event if event == SyncEvent::Timer as u32 => self.set_client_timer(&mut cursor)?,
                _ => {
                    error!("Unknown sync event!");
                    return Err(Box::new(Error::UnknownSyncEvent));
                }
            };


        }
    }

    fn set_client_timer(&mut self, cursor: &mut Cursor<Vec<u8>>) -> result::Result<(), Box<error::Error>> {
        let client_number = cursor.read_u64::<LittleEndian>()?;
        let timer = unsafe { &mut *mem::transmute::<usize, *mut EventTimer>(cursor.read_u64::<LittleEndian>()? as usize) };
        let seconds = cursor.read_i32::<LittleEndian>()?;
        let microseconds = cursor.read_i32::<LittleEndian>()?;
        match self.clients.get_mut().unwrap().get_mut(&client_number) {
            Some(_) => timer.start_timer(seconds, microseconds)?,
            None => error!("Set timer. Client not found!"),
        }

        Ok(())
    }

    pub fn disconnect_client(&mut self, client_number: u64) {
        debug!("Client disconnect {}!", client_number);

        self.clients.get_mut().unwrap().remove(&client_number);
        self.threads.remove(&client_number);
    }

    pub fn get_host(&self) -> &str {
        self.settings.host_for_client.as_ref()
    }

    pub fn get_file_by_path(&self, path: &str) -> String {
        match self.settings.sources.get(path) {
            Some(path) => path.to_string(),
            None => String::new(),
        }
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        debug!("~Server()");

        for (_client_number, thread) in &mut self.threads {
            match thread.take() {
                Some(thread) => match thread.join() {
                    Ok(_) => (),
                    Err(e) => error!("Thread join error! {:?}", e),
                },
                None => error!("Thread not found!"),
            }
        }

        unsafe { SERVER = null_mut(); };
    }
}