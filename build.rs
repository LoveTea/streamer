use std::env;
use std::path::Path;

fn main() {
    let dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    println!("cargo:rustc-link-lib=streamer_ffi");
    println!("cargo:rustc-link-search=native={}", Path::new(&dir).join("streamer_ffi").display());
    println!("cargo:rustc-env=LD_LIBRARY_PATH={}", Path::new(&dir).join("streamer_ffi").display());
}